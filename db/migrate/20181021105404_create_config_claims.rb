# frozen_string_literal: true

class CreateConfigClaims < ActiveRecord::Migration[5.2]
  def change
    create_table :config_claims, id: :uuid do |t|
      t.references :service, foreign_key: true, null: false, type: :uuid
      t.references :config, foreign_key: true, null: false, type: :uuid
      t.string :target, null: false

      t.index %i[service_id target], unique: true

      t.timestamps
    end
  end
end
