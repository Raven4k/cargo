# frozen_string_literal: true

class AddProductionEnvironmentNameToStacks < ActiveRecord::Migration[5.2]
  def change
    add_column :stacks, :production_environment_name, :string, default: :production, null: false
  end
end
