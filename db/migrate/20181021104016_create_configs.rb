# frozen_string_literal: true

class CreateConfigs < ActiveRecord::Migration[5.2]
  def change
    create_table :configs, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.integer :config_type, null: false, default: 0
      t.string :name, null: false

      t.text :encrypted_value, null: false
      t.string :encrypted_value_iv, null: false

      t.index %i[stack_id name], unique: true
      t.index :encrypted_value_iv, unique: true

      t.timestamps
    end
  end
end
