# frozen_string_literal: true

class AddNodeLabelToEnvironments < ActiveRecord::Migration[6.0]
  def change
    add_column :environments, :node_label, :string
  end
end
