# frozen_string_literal: true

class AddLanguageToConfigs < ActiveRecord::Migration[5.2]
  def change
    add_column :configs, :language, :string, default: "text", null: false
  end
end
