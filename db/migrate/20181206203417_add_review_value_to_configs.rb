# frozen_string_literal: true

class AddReviewValueToConfigs < ActiveRecord::Migration[5.2]
  def change
    change_table :configs, bulk: true do |t|
      t.text :encrypted_review_value
      t.string :encrypted_review_value_iv

      t.index :encrypted_review_value_iv, unique: true
    end
  end
end
