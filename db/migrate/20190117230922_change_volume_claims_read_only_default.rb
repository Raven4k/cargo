# frozen_string_literal: true

class ChangeVolumeClaimsReadOnlyDefault < ActiveRecord::Migration[5.2]
  def up
    change_column_default(:volume_claims, :read_only, from: true, to: false)
  end
end
