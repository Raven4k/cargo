# frozen_string_literal: true

class AddHostAndTraefikNetworkAndBackendNameAndPrometheusHostToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    change_table :application_settings, bulk: true do |t|
      t.string :host, default: "localhost", null: false
      t.string :traefik_network, default: "traefik", null: false
      t.string :backend_name, default: "cargo-cargo-web", null: false
      t.boolean :prometheus, default: false, null: false
      t.text :encrypted_prometheus_host
      t.string :encrypted_prometheus_host_iv
    end
  end
end
