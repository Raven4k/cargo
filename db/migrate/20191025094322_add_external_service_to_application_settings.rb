# frozen_string_literal: true

class AddExternalServiceToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    change_table :application_settings, bulk: true do |t|
      t.boolean :minio, default: false, null: false
      t.string :minio_host
      t.text :encrypted_minio_access_key
      t.string :encrypted_minio_access_key_iv
      t.text :encrypted_minio_secret_key
      t.string :encrypted_minio_secret_key_iv

      t.index :encrypted_minio_access_key_iv, unique: true
      t.index :encrypted_minio_secret_key_iv, unique: true
    end
  end
end
