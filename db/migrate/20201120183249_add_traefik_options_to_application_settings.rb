# frozen_string_literal: true

class AddTraefikOptionsToApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    change_table :application_settings, bulk: true do |t|
      t.integer :traefik_version
      t.string :traefik_default_entrypoints
      t.string :traefik_cert_resolvers, array: true
    end

    change_column_null :application_settings, :backend_name, true
  end
end
