# frozen_string_literal: true

class ChangeDefaultServiceDeployOptions < ActiveRecord::Migration[5.2]
  def up
    change_column_default(:services, :restart_delay, from: 5, to: 60) # rubocop:disable Rails/BulkChangeTable
    change_column_default(:services, :restart_window, from: 5, to: 60)

    change_column_default(:services, :rollback_delay, from: 5, to: 60)
    change_column_default(:services, :rollback_monitor, from: 5, to: 60)

    change_column_default(:services, :update_delay, from: 5, to: 60)
    change_column_default(:services, :update_monitor, from: 5, to: 60)
  end
end
