# frozen_string_literal: true

class CreateStackMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :stack_members, id: :uuid do |t|
      t.references :stack, foreign_key: true, type: :uuid
      t.references :user, foreign_key: true, type: :uuid

      t.index %i[stack_id user_id], unique: true

      t.timestamps
    end
  end
end
