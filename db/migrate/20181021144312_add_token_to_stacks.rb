# frozen_string_literal: true

class AddTokenToStacks < ActiveRecord::Migration[5.2]
  def change
    add_column :stacks, :token, :string
    add_index :stacks, :token, unique: true
  end
end
