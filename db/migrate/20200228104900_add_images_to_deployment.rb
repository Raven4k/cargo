# frozen_string_literal: true

class AddImagesToDeployment < ActiveRecord::Migration[6.0]
  def change
    add_column :deployments, :images, :text
  end
end
