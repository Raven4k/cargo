# frozen_string_literal: true

class AddTraefikOptionsToFrontends < ActiveRecord::Migration[6.0]
  def change
    change_table :frontends, bulk: true do |t|
      t.string :router_rule
      t.boolean :tls, default: false, null: false
      t.string :tls_cert_resolver
      t.text :tls_domains, array: true
      t.string :tls_options
    end
  end
end
