# frozen_string_literal: true

class CreateDeployments < ActiveRecord::Migration[5.2]
  def change
    create_table :deployments, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.references :environment, foreign_key: true, null: false, type: :uuid
      t.string :version
      t.text :tags
      t.text :template
      t.string :message

      t.timestamps
    end

    add_reference :environments, :last_deployment, type: :uuid
    add_foreign_key :environments, :deployments, column: :last_deployment_id
  end
end
