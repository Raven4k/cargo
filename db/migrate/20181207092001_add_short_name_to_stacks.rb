# frozen_string_literal: true

class AddShortNameToStacks < ActiveRecord::Migration[5.2]
  def up
    add_column :stacks, :short_name, :string

    execute "UPDATE stacks SET short_name = name"

    change_column_null :stacks, :short_name, null: false
    add_index :stacks, :short_name, unique: true
  end

  def down
    remove_column :stacks, :short_name
  end
end
