# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_22_151301) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "application_settings", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.boolean "allow_creation_of_stacks", default: true, null: false
    t.string "default_production_environment_name", default: "production", null: false
    t.string "user_default_approved_regex"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "host", default: "localhost", null: false
    t.string "traefik_network", default: "traefik", null: false
    t.string "backend_name", default: "cargo-cargo-web"
    t.boolean "prometheus", default: false, null: false
    t.text "encrypted_prometheus_host"
    t.string "encrypted_prometheus_host_iv"
    t.integer "frontend_host_schema", default: 0, null: false
    t.boolean "minio", default: false, null: false
    t.string "minio_host"
    t.text "encrypted_minio_access_key"
    t.string "encrypted_minio_access_key_iv"
    t.text "encrypted_minio_secret_key"
    t.string "encrypted_minio_secret_key_iv"
    t.string "primary_color", null: false
    t.string "node_labels", array: true
    t.string "default_production_node_label"
    t.string "default_review_node_label"
    t.integer "traefik_version"
    t.string "traefik_default_entrypoints"
    t.string "traefik_cert_resolvers", array: true
    t.string "table_options", default: ["striped"], array: true
    t.index ["encrypted_minio_access_key_iv"], name: "index_application_settings_on_encrypted_minio_access_key_iv", unique: true
    t.index ["encrypted_minio_secret_key_iv"], name: "index_application_settings_on_encrypted_minio_secret_key_iv", unique: true
  end

  create_table "config_claims", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "service_id", null: false
    t.uuid "config_id", null: false
    t.string "target", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["config_id"], name: "index_config_claims_on_config_id"
    t.index ["service_id", "target"], name: "index_config_claims_on_service_id_and_target", unique: true
    t.index ["service_id"], name: "index_config_claims_on_service_id"
  end

  create_table "configs", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.integer "config_type", default: 0, null: false
    t.string "name", null: false
    t.text "encrypted_value", null: false
    t.string "encrypted_value_iv", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "encrypted_review_value"
    t.string "encrypted_review_value_iv"
    t.string "language", default: "text", null: false
    t.integer "lock_version", default: 0, null: false
    t.index ["encrypted_review_value_iv"], name: "index_configs_on_encrypted_review_value_iv", unique: true
    t.index ["encrypted_value_iv"], name: "index_configs_on_encrypted_value_iv", unique: true
    t.index ["stack_id", "name"], name: "index_configs_on_stack_id_and_name", unique: true
    t.index ["stack_id"], name: "index_configs_on_stack_id"
  end

  create_table "deployments", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.uuid "environment_id", null: false
    t.string "version"
    t.text "tags"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "encrypted_template"
    t.string "encrypted_template_iv"
    t.text "images"
    t.index ["environment_id"], name: "index_deployments_on_environment_id"
    t.index ["stack_id"], name: "index_deployments_on_stack_id"
  end

  create_table "environments", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.string "name", null: false
    t.datetime "deployed_at"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "last_deployment_id"
    t.string "node_label"
    t.index ["last_deployment_id"], name: "index_environments_on_last_deployment_id"
    t.index ["stack_id", "name", "deleted_at"], name: "index_environments_on_stack_id_and_name_and_deleted_at", unique: true
    t.index ["stack_id"], name: "index_environments_on_stack_id"
  end

  create_table "external_service_claims", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "external_service_id", null: false
    t.uuid "service_id", null: false
    t.string "target", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["external_service_id", "service_id"], name: "index_external_service_claims_on_es_id_and_s_id", unique: true
    t.index ["external_service_id"], name: "index_external_service_claims_on_external_service_id"
    t.index ["service_id", "target"], name: "index_external_service_claims_on_service_id_and_target", unique: true
    t.index ["service_id"], name: "index_external_service_claims_on_service_id"
  end

  create_table "external_service_environment_claims", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "external_service_id"
    t.uuid "environment_id", null: false
    t.text "encrypted_settings"
    t.string "encrypted_settings_iv"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["encrypted_settings_iv"], name: "index_external_service_environment_claims_on_e_s_iv", unique: true
    t.index ["environment_id"], name: "index_external_service_environment_claims_on_environment_id"
    t.index ["external_service_id", "environment_id"], name: "index_external_service_environment_claims_on_e_s_id_and_e_id", unique: true
    t.index ["external_service_id"], name: "index_external_service_environment_claims_on_e_s_id"
  end

  create_table "external_services", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.integer "service_type", default: 0, null: false
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "minio_production_bucket_name"
    t.index ["minio_production_bucket_name"], name: "index_external_services_on_minio_production_bucket_name", unique: true
    t.index ["stack_id", "name"], name: "index_external_services_on_stack_id_and_name", unique: true
    t.index ["stack_id"], name: "index_external_services_on_stack_id"
  end

  create_table "frontends", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.uuid "service_id", null: false
    t.string "name", null: false
    t.integer "port", null: false
    t.text "hosts"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "redirect", default: false
    t.string "redirect_regex"
    t.string "redirect_replacement"
    t.boolean "redirect_permanent", default: true
    t.boolean "auth_basic", default: false
    t.boolean "auth_basic_remove_header", default: true
    t.text "encrypted_auth_basic_users"
    t.string "encrypted_auth_basic_users_iv"
    t.text "request_headers"
    t.text "response_headers"
    t.boolean "hsts", default: true
    t.integer "hsts_max_age", default: 15552000
    t.boolean "hsts_include_subdomain", default: false
    t.string "router_rule"
    t.boolean "tls", default: false, null: false
    t.string "tls_cert_resolver"
    t.text "tls_domains", array: true
    t.string "tls_options"
    t.index ["service_id"], name: "index_frontends_on_service_id"
    t.index ["stack_id", "name"], name: "index_frontends_on_stack_id_and_name", unique: true
    t.index ["stack_id"], name: "index_frontends_on_stack_id"
  end

  create_table "services", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.string "name", null: false
    t.string "image", null: false
    t.string "command"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "replicas", default: 1, null: false
    t.integer "restart_condition", default: 1, null: false
    t.integer "restart_delay", default: 60, null: false
    t.integer "restart_window", default: 60, null: false
    t.integer "rollback_parallelism", default: 1, null: false
    t.integer "rollback_delay", default: 60, null: false
    t.integer "rollback_failure_action", default: 0, null: false
    t.integer "rollback_monitor", default: 60, null: false
    t.integer "rollback_order", default: 1, null: false
    t.integer "update_parallelism", default: 1, null: false
    t.integer "update_delay", default: 60, null: false
    t.integer "update_failure_action", default: 2, null: false
    t.integer "update_monitor", default: 60, null: false
    t.integer "update_order", default: 1, null: false
    t.boolean "healthcheck", default: false, null: false
    t.text "encrypted_healthcheck_test"
    t.string "encrypted_healthcheck_test_iv"
    t.integer "healthcheck_interval", default: 30, null: false
    t.integer "healthcheck_timeout", default: 30, null: false
    t.integer "healthcheck_start_period", default: 0, null: false
    t.integer "healthcheck_retries", default: 3, null: false
    t.index ["stack_id", "name"], name: "index_services_on_stack_id_and_name", unique: true
    t.index ["stack_id"], name: "index_services_on_stack_id"
  end

  create_table "stack_members", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stack_id", "user_id"], name: "index_stack_members_on_stack_id_and_user_id", unique: true
    t.index ["stack_id"], name: "index_stack_members_on_stack_id"
    t.index ["user_id"], name: "index_stack_members_on_user_id"
  end

  create_table "stacks", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "token"
    t.string "registry_url"
    t.string "registry_username"
    t.string "encrypted_registry_password"
    t.string "encrypted_registry_password_iv"
    t.string "short_name"
    t.string "production_environment_name", default: "production", null: false
    t.string "default_production_node_label"
    t.string "default_review_node_label"
    t.index ["name"], name: "index_stacks_on_name", unique: true
    t.index ["short_name"], name: "index_stacks_on_short_name", unique: true
    t.index ["token"], name: "index_stacks_on_token", unique: true
  end

  create_table "users", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.string "username", null: false
    t.string "oidc_uid", null: false
    t.string "oidc_token", null: false
    t.boolean "admin", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "approved", default: false, null: false
    t.index ["oidc_token"], name: "index_users_on_oidc_token", unique: true
    t.index ["oidc_uid"], name: "index_users_on_oidc_uid", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "volume_claims", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "service_id", null: false
    t.uuid "volume_id", null: false
    t.string "target", null: false
    t.boolean "read_only", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["service_id", "target"], name: "index_volume_claims_on_service_id_and_target", unique: true
    t.index ["service_id", "volume_id"], name: "index_volume_claims_on_service_id_and_volume_id", unique: true
    t.index ["service_id"], name: "index_volume_claims_on_service_id"
    t.index ["volume_id"], name: "index_volume_claims_on_volume_id"
  end

  create_table "volumes", id: :uuid, default: -> { "public.gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "stack_id", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stack_id", "name"], name: "index_volumes_on_stack_id_and_name", unique: true
    t.index ["stack_id"], name: "index_volumes_on_stack_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "config_claims", "configs"
  add_foreign_key "config_claims", "services"
  add_foreign_key "configs", "stacks"
  add_foreign_key "deployments", "environments"
  add_foreign_key "deployments", "stacks"
  add_foreign_key "environments", "deployments", column: "last_deployment_id"
  add_foreign_key "environments", "stacks"
  add_foreign_key "external_service_claims", "external_services"
  add_foreign_key "external_service_claims", "services"
  add_foreign_key "external_service_environment_claims", "environments"
  add_foreign_key "external_service_environment_claims", "external_services"
  add_foreign_key "external_services", "stacks"
  add_foreign_key "frontends", "services"
  add_foreign_key "frontends", "stacks"
  add_foreign_key "services", "stacks"
  add_foreign_key "stack_members", "stacks"
  add_foreign_key "stack_members", "users"
  add_foreign_key "volume_claims", "services"
  add_foreign_key "volume_claims", "volumes"
  add_foreign_key "volumes", "stacks"
end
