# frozen_string_literal: true

if Rails.env.development? && Cargo.config.oidc_host.blank?
  puts "WARNING! You must specify the OpenID Connect configuration, otherwise the application will not work like expected." # rubocop:disable Rails/Output
else
  Rails.application.config.middleware.use OmniAuth::Builder do
    openid_host_uri = URI(Cargo.config.oidc_host) rescue nil # rubocop:disable Style/RescueModifier

    provider(
      :openid_connect,
      issuer: Cargo.config.oidc_issuer,
      discovery: true,
      scope: %w[openid profile email],
      client_options: {
        host: openid_host_uri&.host,
        scheme: openid_host_uri&.scheme,
        port: openid_host_uri&.port,
        identifier: Cargo.config.oidc_identifier,
        secret: Cargo.config.oidc_secret
      }
    )
  end
end

OmniAuth.config.on_failure = proc { |env|
  OmniAuth::FailureEndpoint.new(env).redirect_to_failure
}

# Monkey patch to get a dynamic redirect_uri
# `callback_url` is defined by Ominauth
OmniAuth::Strategies::OpenIDConnect.class_eval do
  alias_method :redirect_uri, :callback_url
end
