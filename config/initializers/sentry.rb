# frozen_string_literal: true

Raven.configure do |config|
  config.environments = %w[staging production]
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  config.silence_ready = true
  config.breadcrumbs_logger = %i[sentry_logger active_support_logger]
  config.release = "cargo@#{Rails.configuration.version}"
  config.async = lambda do |event|
    Thread.new { Raven.send_event(event) }
  end
end
