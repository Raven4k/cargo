const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const vue = require('./loaders/vue')

const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

environment.plugins.prepend(
  'MomentLocalesPlugin',
  new MomentLocalesPlugin({
    localesToKeep: ['de'],
  })
)

environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.loaders.prepend('vue', vue)
environment.config.resolve.alias = { 'vue$': 'vue/dist/vue.esm.js' }

module.exports = environment
