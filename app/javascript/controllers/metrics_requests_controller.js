import MetricsController from "./metrics_controller"

export default class extends MetricsController {
  datasets = [{
    label: "1xx",
    color: "#adb5bd"
  }, {
    label: "2xx",
    color: "#28a745"
  }, {
    label: "3xx",
    color: "#007bff"
  }, {
    label: "4xx",
    color: "#ffc107"
  }, {
    label: "5xx",
    color: "#dc3545"
  }]

  findDataset(metric){
    return this.chart.data.datasets.find((dataset) => dataset.label == metric.status)
  }
}
