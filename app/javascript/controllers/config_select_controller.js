import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "hint", "select", "input" ]

  connect() {
    this.updateHint()
    this.updateInput()
  }

  changedOption() {
    this.updateHint()
    this.updateInput()
  }

  updateHint() {
    switch(this.group()) {
      case "config": {
        this.hintTarget.textContent = "/target/config"
        break
      }
      case "environment": {
        this.hintTarget.textContent = "ENVIRONMENT_VARIABLE"
        break
      }
      case "secret": {
        this.hintTarget.textContent = "/run/secret/<target>"
        break
      }
      case "minio": {
        this.hintTarget.textContent = "It will add the following envrionment variables: <target>_HOST, <target>_BUCKET, <target>_ACCESS_KEY, <target>_SECRET_KEY"
        break
      }
      default: {
        this.hintTarget.textContent = ""
      }
    }
  }

  updateInput(){
    if (this.inputTarget.value.length > 0) return;

    switch(this.group()) {
      case "config":
      case "secret":
        this.inputTarget.value = this.option().textContent.toLowerCase()
        break

      case "environment":
      case "minio":
        this.inputTarget.value = this.option().textContent.toUpperCase().replace(/-|_/ig, "_")
        break
    }
  }

  option() {
    return this.selectTarget.options[this.selectTarget.selectedIndex]
  }

  group() {
    return this.option().parentElement.label
  }
}
