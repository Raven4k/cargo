import MetricsController from "./metrics_controller"

export default class extends MetricsController {
  legend = false

  datasets = [{
    label: "Duration",
    color: "#007bff"
  }]
}
