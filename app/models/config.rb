# frozen_string_literal: true

class Config < ApplicationRecord
  LANGUAGES = %w[text cmake html javascript json jinja2 markdown nginx php python ruby shell toml xml yaml].sort.freeze
  enum config_type: { environment: 0, secret: 1, config: 2 }

  attr_readonly :stack_id
  attr_readonly :config_type
  attr_readonly :name

  belongs_to :stack
  has_many :config_claims, dependent: :restrict_with_error

  scope :ordered, -> { order(Arel.sql('LOWER("configs"."name") ASC')) }

  attr_encrypted :value, key: Rails.application.key_generator.generate_key("Config#value", 32)
  attr_encrypted :review_value, key: Rails.application.key_generator.generate_key("Config#review_value", 32)

  validates :config_type, presence: true
  validates :config_type, inclusion: { in: config_types.keys }
  validates :name, presence: true
  validates :name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :name, uniqueness: { case_sensitive: false, scope: :stack_id }
  validates :value, presence: true
  validates :language, presence: true
  validates :language, inclusion: { in: LANGUAGES }

  after_create :docker_create
  after_update :docker_create
  after_destroy :docker_remove

  def docker_name(production: true)
    # Adding version because Docker CLI doesn't support an update
    # Configs and Secrets should be immutable although there is an API
    # https://github.com/moby/moby/issues/29882
    if production
      "#{docker_name_base}production_#{lock_version}"
    else
      "#{docker_name_base}review_#{lock_version}"
    end
  end

  def docker_name_base
    "#{stack.docker_name}_#{name}_".downcase
  end

  def formatted_value
    format(value, stack.config_options)
  end

  def formatted_review_value
    format(review_value, stack.config_review_options).presence || formatted_value
  end

  def docker_create
    case config_type.to_sym
    when :config
      DockerCli::Config.create(name: docker_name(production: true), value: formatted_value)
      DockerCli::Config.create(name: docker_name(production: false), value: formatted_review_value)
    when :secret
      DockerCli::Secret.create(name: docker_name(production: true), value: formatted_value)
      DockerCli::Secret.create(name: docker_name(production: false), value: formatted_review_value)
    end
  end

  def docker_remove
    case config_type.to_sym
    when :config
      DockerCli::Config.remove(name: docker_name_base)
    when :secret
      DockerCli::Secret.remove(name: docker_name_base)
    end
  end

private

  def format(value, options)
    Kernel.format(value, options)
  rescue StandardError => exception
    Rails.logger.warn "FormattedValueException: #{exception.class}: #{exception.message}"
    value
  end
end
