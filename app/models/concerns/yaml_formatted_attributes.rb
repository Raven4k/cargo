# frozen_string_literal: true

module YamlFormattedAttributes
  extend ActiveSupport::Concern

  class_methods do
    def yaml_formatted_attribute(attribute)
      define_yaml_formatted_attribute_setter(attribute)
      define_yaml_formatted_attribute_getter(attribute)

      validate { validate_yaml_formatted_attribute(attribute) }
    end

  private

    def define_yaml_formatted_attribute_setter(attribute)
      define_method("#{attribute}=") do |value|
        value = YAML.safe_load(value) if value.is_a? String
        value = value.deep_transform_keys { |key| key.to_s.downcase } if value.respond_to?(:deep_transform_keys)
        super(value)
      rescue Psych::SyntaxError
        super(value)
      end
    end

    def define_yaml_formatted_attribute_getter(attribute)
      define_method("#{attribute}_yaml") do
        value = public_send(attribute)

        return "" if value.blank?
        return value if value.is_a?(String)

        value.to_yaml[4..-2]
      end
    end
  end

private # rubocop:disable Lint/UselessAccessModifier

  def validate_yaml_formatted_attribute(attribute)
    return if public_send(attribute).nil?
    return if public_send(attribute).is_a?(Hash)

    errors.add(attribute, :invalid)
  end
end
