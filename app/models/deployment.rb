# frozen_string_literal: true

class Deployment < ApplicationRecord
  include YamlFormattedAttributes

  serialize :tags
  serialize :images

  attr_encrypted :template, key: Rails.application.key_generator.generate_key("Deployment#template", 32), allow_empty_value: true, marshal: true

  belongs_to :stack
  belongs_to :environment
  has_one :last_deployment_environment, dependent: :nullify, class_name: "Environment", foreign_key: :last_deployment_id, inverse_of: :last_deployment

  scope :ordered, -> { order(created_at: :desc) }

  validates :tags, absence: true, if: :images?
  validates :images, absence: true, if: :tags?
  validates :deleted_at, absence: true

  before_validation :set_stack
  before_create :set_template
  after_create :deploy_stack
  after_commit :destroy_unneeded_external_service_environment_claims

  delegate :name, :docker_name, :deleted_at, :production?, to: :environment, allow_nil: true

  yaml_formatted_attribute :tags
  yaml_formatted_attribute :images

  def deploy_stack # rubocop:disable Metrics/AbcSize
    return false if deleted_at.present?

    message = []
    message << DockerCli.login(stack.registry) if stack.registry?
    message << DockerCli::Stack.deploy(name: docker_name, docker_compose: template)

    environment.update!(last_deployment: self, deployed_at: Time.current)
    update!(message: message.join("\n"))
  end

  def environment=(environment_or_name)
    if environment_or_name.is_a? String
      raise "Stack is missing" if stack.blank?

      self.environment = stack.environments.not_deleted.find_or_create_by(name: environment_or_name)
    else
      super(environment_or_name)
    end
  end

  def message_short
    message_short = message.to_s.split("\n")[0..1].reject { |line| line.squish.blank? }.join("\n")
    message_short += " ..." if message_short.length < message.to_s.length
    message_short
  end

  def frontend_hosts
    template.fetch("services", {}).map do |_, service|
      service.fetch("deploy", {}).fetch("labels", {}).map do |name, value|
        if Current.settings.traefik_version == "v2"
          frontend_hosts_traefik_v2(name, value)
        else
          frontend_hosts_traefik_v1(name, value)
        end
      end
    end.flatten.compact
  end

private

  def frontend_hosts_traefik_v1(name, value)
    return unless name.match?(/traefik\.\w+\.frontend\.rule/)

    value.split(":").last.split(",")
  end

  def frontend_hosts_traefik_v2(name, value)
    return unless name.match?(/traefik\.http\.routers\..+\.rule/)
    return unless value.start_with?("Host(")

    value.scan(/`(.+?)`/).flatten
  end

  def destroy_unneeded_external_service_environment_claims
    environment.external_service_environment_claims.where(external_service: nil).destroy_all
  end

  def set_template
    self.template = DockerComposeService.new(deployment: self).content
  rescue DockerComposeService::Error, MinioCli::Error => exception
    errors.add(:base, exception.message)

    throw :abort
  end

  def set_stack
    return if stack.present?
    return if environment.blank?

    self.stack = environment.stack
  end
end
