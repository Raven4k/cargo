# frozen_string_literal: true

class ExternalService < ApplicationRecord
  enum service_type: { minio: 0 }

  attr_readonly :stack_id
  attr_readonly :service_type
  attr_readonly :name

  scope :ordered, -> { order('LOWER("external_services"."name") ASC') }

  belongs_to :stack, inverse_of: :external_services
  has_many :external_service_claims, dependent: :restrict_with_error
  has_many :external_service_environment_claims, dependent: :nullify

  validates :service_type, presence: true
  validates :service_type, inclusion: { in: service_types.keys }
  validates :name, presence: true
  validates :name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :name, uniqueness: { case_sensitive: false, scope: :stack_id }, if: :will_save_change_to_name?
  validates :minio_production_bucket_name, uniqueness: { case_sensitive: false }, allow_nil: true, if: :will_save_change_to_minio_production_bucket_name?
  validates :minio_production_bucket_name, format: { with: /\A[0-9a-z]{1}[0-9a-z\-]+\z/ }, allow_nil: true
  validates :minio_production_bucket_name, length: { in: 3..63 }, allow_nil: true

  def minio_production_bucket_name=(value)
    super(value.presence)
  end
end
