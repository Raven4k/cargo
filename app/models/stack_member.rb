# frozen_string_literal: true

class StackMember < ApplicationRecord
  belongs_to :stack
  belongs_to :user

  validates :user_id, uniqueness: { scope: [:stack_id] }
end
