# frozen_string_literal: true

class ConfigClaim < ApplicationRecord
  belongs_to :service
  belongs_to :config

  scope :environments, -> { config_type(:environment) }
  scope :secrets, -> { config_type(:secret) }
  scope :configs, -> { config_type(:config) }
  scope :config_type, ->(type) { joins(:config).where(configs: { config_type: type }) }
  scope :ordered, -> { joins(:config).merge(Config.ordered) }

  validates :target, presence: true
  validates :target, uniqueness: { case_sensitive: false, scope: :service_id }
  validates :target, format: { without: %r{/|:} }, if: :environment?
  validates :target, format: { without: %r{/|:} }, if: :secret?
  validates :target, format: { without: /:/ }, if: :config?

  delegate :environment?, :secret?, :config?, to: :config, allow_nil: true

  def stack
    service&.stack || config&.stack
  end
end
