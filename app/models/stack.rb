# frozen_string_literal: true

class Stack < ApplicationRecord
  attr_readonly :short_name

  attribute :production_environment_name, default: -> { Current.settings.default_production_environment_name }
  attribute :default_production_node_label, default: -> { Current.settings.default_production_node_label }
  attribute :default_review_node_label, default: -> { Current.settings.default_review_node_label }

  attr_encrypted :registry_password, key: Rails.application.key_generator.generate_key("Stack#registry_password", 32)

  has_many :stack_members, dependent: :destroy
  has_many :users, through: :stack_members

  has_many :services, dependent: :destroy
  has_many :volumes, dependent: :destroy
  has_many :frontends, dependent: :destroy
  has_many :configs, dependent: :destroy
  has_many :environments, dependent: :destroy
  has_many :deployments, dependent: :destroy
  has_many :external_services, dependent: :destroy

  scope :ordered, -> { order(Arel.sql('LOWER("stacks"."name") ASC')) }

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :short_name, presence: true
  validates :short_name, uniqueness: { case_sensitive: false }
  validates :short_name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :token, presence: true
  validates :token, uniqueness: { case_sensitive: false }

  validates :production_environment_name, presence: true

  validates :registry_url, presence: true, if: :registry?
  validates :registry_username, presence: true, if: :registry?
  validates :registry_password, presence: true, if: :registry?

  validate :validate_default_node_lables

  before_validation :set_token

  def docker_name
    "#{Cargo.config.stack_prefix}_#{short_name.downcase}"
  end

  def config_options
    @config_options ||= fetch_config_options(:value)
  end

  def config_review_options
    @config_review_options ||= fetch_config_options(:review_value).compact.reverse_merge(config_options)
  end

  def registry
    {
      url: registry_url,
      username: registry_username,
      password: registry_password
    }
  end

  def registry?
    registry_url.present? || registry_username.present? || registry_password.present?
  end

  def production_environment
    environments.not_deleted.find_by(name: production_environment_name)
  end

  def fresh?
    !(services.any? || configs.any? || volumes.any?)
  end

private

  def set_token
    return if token.present?

    self.token = SecureRandom.hex(32)
  end

  def fetch_config_options(field)
    options = configs.map { |config| [config.name, config.public_send(field)] }.to_h.symbolize_keys
    options.default_proc = proc { |_, key| "%{#{key}}" }
    options
  end

  def validate_default_node_lables
    errors.add(:default_production_node_label, :inclusion) if default_production_node_label? && Current.settings.node_labels.exclude?(default_production_node_label)
    errors.add(:default_review_node_label, :inclusion) if default_review_node_label? && Current.settings.node_labels.exclude?(default_review_node_label)
  end
end
