# frozen_string_literal: true

class Current < ActiveSupport::CurrentAttributes
  attribute :user
  attribute :settings

  def settings
    super || self.settings = ApplicationSetting.get
  end
end
