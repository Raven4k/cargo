# frozen_string_literal: true

class Environment < ApplicationRecord
  attr_readonly :name

  belongs_to :stack
  belongs_to :last_deployment, optional: true, class_name: "Deployment"

  has_many :deployments, dependent: :destroy
  has_many :external_service_environment_claims, dependent: :destroy

  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :ordered, -> { order(Arel.sql('LOWER("environments"."name") ASC')) }

  validates :stack, presence: true
  validates :name, presence: true
  validates :name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :name, uniqueness: { case_sensitive: false, scope: %i[stack_id deleted_at] }
  validate :validate_node_label

  before_validation :set_node_label

  before_destroy :remove_stack

  def production?
    stack.production_environment_name.casecmp(name)&.zero?
  end

  def docker_name
    "#{stack.docker_name}_#{name}".downcase
  end

  def remove_stack
    DockerCli::Stack.remove(name: docker_name)

    external_service_environment_claims.destroy_all

    update!(deleted_at: Time.current)
  rescue StandardError => exception
    Raven.capture_exception(exception, level: :warning)

    false
  end

  def docker_api
    @docker_api ||= DockerApi::Stack.new(docker_name)
  end

  def docker_services
    docker_api.services
  end

  def docker_tasks
    docker_api.tasks
  end

  def docker_containers
    docker_api.containers
  end

private

  def set_node_label
    return if node_label?
    return if stack.blank?

    self.node_label = production? ? stack.default_production_node_label : stack.default_review_node_label
  end

  def validate_node_label
    return unless node_label?

    errors.add(:node_label, :inclusion) if Current.settings.node_labels.exclude?(node_label)
  end
end
