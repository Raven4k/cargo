# frozen_string_literal: true

class ServicesController < ApplicationController
  before_action :set_stack, only: %i[index new create]
  before_action :set_service, only: %i[show duplicate edit update destroy]

  def index
    authorize Service, :index?

    @services = @stack.services.ordered
  end

  def show
    authorize @service, :show?
  end

  def new
    @service = @stack.services.new

    authorize @service, :create?
  end

  def edit
    authorize @service, :update?
  end

  def duplicate
    authorize @service, :duplicate?

    duplicated_service = @service.duplicate

    if duplicated_service.save
      redirect_to duplicated_service, flash: { success: t(".success") }
    else
      redirect_to @service, alert: duplicated_service.errors.full_messages.to_sentence
    end
  end

  def create
    @service = @stack.services.new(service_params)

    authorize @service, :create?

    if @service.save
      redirect_to @service, flash: { success: t(".success") }
    else
      render :new
    end
  end

  def update
    authorize @service, :update?

    if @service.update(service_params)
      redirect_to @service, flash: { success: t(".success") }
    else
      render :edit
    end
  end

  def destroy
    authorize @service, :destroy?

    if @service.destroy
      redirect_to [@service.stack, :services], flash: { success: t(".success") }
    else
      redirect_to @service, alert: t(".alert")
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_service
    @service = Service.find(params[:id])
    @stack = @service.stack

    authorize @stack, :show?
  end

  def service_params
    params.require(:service).permit(
      :name, :image, :command,
      :replicas, :restart_condition, :restart_delay, :restart_window,
      :rollback_parallelism, :rollback_delay, :rollback_failure_action, :rollback_monitor, :rollback_order,
      :update_parallelism, :update_delay, :update_failure_action, :update_monitor, :update_order,
      :healthcheck, :healthcheck_test, :healthcheck_interval, :healthcheck_timeout, :healthcheck_start_period, :healthcheck_retries,
      config_claims_attributes: %i[id config_id target _destroy],
      external_service_claims_attributes: %i[id external_service_id target _destroy],
      volume_claims_attributes: %i[id volume_id target read_only _destroy]
    )
  end
end
