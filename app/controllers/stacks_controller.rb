# frozen_string_literal: true

class StacksController < ApplicationController
  before_action :set_stack, only: %i[show edit update destroy]

  def index
    authorize Stack, :index?

    @stacks = Stack.ordered
  end

  def show
    authorize @stack, :show?

    @environments = @stack.environments.ordered.not_deleted.includes(:last_deployment).to_a
    @production = @environments.find(&:production?)
    @reviews = @environments.reject(&:production?)
  end

  def new
    @stack = current_user.stacks.new

    authorize @stack, :create?
  end

  def edit
    authorize @stack, :update?
  end

  def create
    @stack = Stack.new(stack_params)
    @stack.users << current_user unless @stack.users.include? current_user

    authorize @stack, :create?

    if @stack.save
      redirect_to @stack, flash: { success: t(".success") }
    else
      render :new
    end
  end

  def update
    authorize @stack, :update?

    if @stack.update(stack_params)
      redirect_to @stack, flash: { success: t(".success") }
    else
      render :edit
    end
  end

  def destroy
    authorize @stack, :destroy?

    if @stack.destroy
      redirect_to [:stacks], flash: { success: t(".success") }
    else
      redirect_to @stack, alert: t(".alert")
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:id])
  end

  def stack_params
    params.require(:stack).permit(
      :name, :short_name, :production_environment_name,
      :default_production_node_label, :default_review_node_label,
      :registry_url, :registry_username, :registry_password,
      user_ids: []
    )
  end
end
