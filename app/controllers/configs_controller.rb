# frozen_string_literal: true

class ConfigsController < ApplicationController
  before_action :set_stack, only: %i[index new create]
  before_action :set_config, only: %i[show edit update destroy]

  def index
    authorize Config, :index?

    @configs = @stack.configs.ordered
  end

  def show
    authorize @config, :show?
  end

  def new
    @config = @stack.configs.new

    authorize @config, :create?
  end

  def edit
    authorize @config, :update?
  end

  def create
    @config = @stack.configs.new(config_params)

    authorize @config, :create?

    if @config.save
      redirect_to @config, flash: { success: t(".success") }
    else
      render :new
    end
  end

  def update
    authorize @config, :update?

    if @config.update(config_params)
      redirect_to @config, flash: { success: t(".success") }
    else
      render :edit
    end
  rescue ActiveRecord::StaleObjectError
    @config.reload
    flash.now[:error] = t(".stale")
    render :edit, status: :conflict
  end

  def destroy
    authorize @config, :destroy?

    if @config.destroy
      redirect_to [@config.stack, :configs], flash: { success: t(".success") }
    else
      redirect_to @config, alert: t(".alert")
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_config
    @config = Config.find(params[:id])
    @stack = @config.stack

    authorize @stack, :show?
  end

  def config_params
    params.require(:config).permit(:config_type, :name, :value, :review_value, :language, :lock_version)
  end
end
