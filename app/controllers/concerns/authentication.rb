# frozen_string_literal: true

module Authentication
  extend ActiveSupport::Concern

  included do
    before_action :authentication!

    helper_method :user_signed_in?
    helper_method :current_user
  end

private

  def authentication!
    self.current_user = User.find_by(id: session[:current_user_id]) || failback_dummy_user

    return if user_signed_in? && current_user.approved?

    respond_to do |format|
      format.html { redirect_to "/auth/openid_connect" }
      format.json { render json: { status: "unauthorized" }, status: :unauthorized }
    end
  end

  def user_signed_in?
    current_user.present?
  end

  def current_user
    Current.user
  end

  def current_user=(user)
    session[:current_user_id] = user&.id

    Current.user = user
  end

  # :nocov:
  def failback_dummy_user
    return unless Rails.env.development?
    return if Cargo.config.oidc_host.present?

    Rails.logger.warn("WARNING! You have to specify the OpenID Connect configuration. Using dummy user as fallback!")
    User.create_with(oidc_uid: :dummy, oidc_token: :dummy, approved: true, admin: true).find_or_create_by!(username: "dummy")
  end
  # :nocov:
end
