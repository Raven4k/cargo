# frozen_string_literal: true

class SessionsController < ApplicationController
  skip_before_action :authentication!

  def create
    self.current_user = User.from_omniauth(auth_hash)

    if user_signed_in?
      if current_user.approved?
        redirect_to request.env["omniauth.origin"] || root_path
      else
        redirect_to_failure alert: t(".not_approved")
      end
    else
      redirect_to_failure
    end
  end

  def failure; end

private

  def auth_hash
    request.env["omniauth.auth"]
  end

  def redirect_to_failure(options = {})
    self.current_user = nil
    redirect_to sessions_failure_path, options
  end
end
