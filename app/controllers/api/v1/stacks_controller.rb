# frozen_string_literal: true

module Api
  module V1
    class StacksController < Api::V1::ApplicationController
      before_action :set_stack

      def deploy
        @development = @stack.deployments.new

        if @development.update deploy_params
          render json: { status: :created }, status: :created
        else
          render json: { status: :failed, errors: @development.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def remove
        @environment = @stack.environments.not_deleted.find_by(name: params[:environment])

        if @environment.blank?
          render json: { status: :failed, errors: ["environment not found"] }, status: :unprocessable_entity
        elsif @environment.production?
          render json: { status: :failed, errors: ["Production environment can not be removed"] }, status: :unprocessable_entity
        elsif @environment.remove_stack
          render json: { status: :stopped }, status: :ok
        else
          render json: { status: :failed }, status: :internal_server_error
        end
      end

    private

      def set_stack
        @stack = Stack.find_by!(id: params[:id], token: request.headers["X-TOKEN"])
      end

      def deploy_params
        params.permit(:environment, :version, tags: {}, images: {})
      end
    end
  end
end
