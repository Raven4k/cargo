# frozen_string_literal: true

class EnvironmentsController < ApplicationController
  before_action :set_stack, only: %i[new create]
  before_action :set_environment, only: %i[show destroy]

  def show
    authorize @environment, :show?
  end

  def new
    @environment = @stack.environments.new

    authorize @environment, :create?
  end

  def create
    @environment = @stack.environments.new(environment_params)

    authorize @environment, :create?

    if @environment.save
      redirect_to [@environment.stack, :deployments], flash: { success: t(".success") }
    else
      render :new
    end
  end

  def destroy
    authorize @environment, :destroy?

    @environment.remove_stack

    redirect_to [@environment.stack]
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_environment
    @environment = Environment.not_deleted.find(params[:id])
    @stack = @environment.stack

    authorize @stack, :show?
  end

  def environment_params
    params.require(:environment).permit(:name, :node_label)
  end
end
