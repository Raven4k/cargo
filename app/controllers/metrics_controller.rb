# frozen_string_literal: true

class MetricsController < ApplicationController
  before_action :set_environment
  before_action :set_service
  before_action :set_metrics

  def requests
    render json: @metrics.requests
  end

  def response_time
    render json: @metrics.response_time
  end

private

  def set_environment
    @environment = Environment.not_deleted.find(params[:environment_id])
    @stack = @environment.stack

    authorize @stack, :show?
    authorize @environment, :metrics?
  end

  def set_service
    @service = @stack.services.find(params[:service_id])

    authorize @service, :show?
  end

  def set_metrics
    @metrics = MetricsService.new(@environment, @service)
  end
end
