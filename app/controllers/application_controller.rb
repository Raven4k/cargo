# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include DefaultUrlOptions
  include Authentication
  include RescueNotAuthorized
  include SentryContext
  include Pundit
end
