# frozen_string_literal: true

module DockerCompose
  class ExternalServiceService
    def initialize(external_service_claim:, deployment:)
      @external_service_claim = external_service_claim
      @external_service = external_service_claim.external_service
      @deployment = deployment
    end

    def environment
      if @external_service.minio?
        minio_environment
      else
        {}
      end
    end

  private

    def minio_environment
      return {} unless Current.settings.minio?

      {
        "#{@external_service_claim.target}_HOST" => Current.settings.minio_host,
        "#{@external_service_claim.target}_BUCKET" => environment_settings[:bucket_name],
        "#{@external_service_claim.target}_ACCESS_KEY" => environment_settings[:access_key],
        "#{@external_service_claim.target}_SECRET_KEY" => environment_settings[:secret_key]
      }
    end

    def external_service_environment_claim
      @external_service_environment_claim ||=
        @external_service
        .external_service_environment_claims
        .find_or_create_by!(environment: @deployment.environment)
    end

    def environment_settings
      external_service_environment_claim.settings
    end
  end
end
