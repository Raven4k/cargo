# frozen_string_literal: true

module DockerCompose
  class ConfigService
    def initialize(config_claim:, deployment:)
      @config_claim = config_claim
      @config = config_claim.config
      @deployment = deployment
    end

    delegate :config?, :environment?, :secret?, to: :@config

    def config
      return unless config?

      {
        source: docker_name,
        target: @config_claim.target
      }
    end

    def environment
      return unless environment?

      {
        @config_claim.target => formatted_value
      }
    end

    def secret
      return unless secret?

      {
        source: docker_name,
        target: @config_claim.target
      }
    end

    def stack_config
      return unless config?

      {
        docker_name => {
          external: true
        }
      }
    end

    def stack_secret
      return unless secret?

      {
        docker_name => {
          external: true
        }
      }
    end

  private

    def docker_name
      @config.docker_name(production: @deployment.production?)
    end

    def formatted_value
      if @deployment.production?
        @config.formatted_value
      else
        @config.formatted_review_value
      end
    end
  end
end
