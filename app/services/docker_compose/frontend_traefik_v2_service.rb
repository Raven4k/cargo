# frozen_string_literal: true

module DockerCompose
  class FrontendTraefikV2Service # rubocop:disable Metrics/ClassLength
    HOSTNAME_REGEXP = /\A((xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}\Z/i.freeze

    def initialize(frontend:, deployment:)
      @frontend = frontend
      @deployment = deployment
      @stack = deployment.stack
      @service = frontend.service
    end

    def labels # rubocop:disable Metrics/MethodLength
      {
        "traefik.enable": "true",
        "traefik.http.services.#{traefik_name}.loadbalancer.server.port": @frontend.port,
        "traefik.http.services.#{traefik_name}.loadbalancer.passhostheader": "true"
      }
        .merge(router)
        .merge(auth_basic)
        .merge(errors)
        .merge(redirect)
        .merge(custom_headers)
        .merge(hsts)
    end

    def environment
      {
        "CARGO_FRONTEND_#{name.upcase}_RULE" => router_rule
      }
    end

  private

    def name
      @frontend.name.downcase
    end

    def traefik_name
      [@deployment.docker_name.tr("_", "-"), @service.name, @frontend.name].join("-").downcase
    end

    def router
      {
        "traefik.http.routers.#{traefik_name}.entryPoints": Current.settings.traefik_default_entrypoints.presence,
        "traefik.http.routers.#{traefik_name}.rule": router_rule,
        "traefik.http.routers.#{traefik_name}.service": traefik_name,
        "traefik.http.routers.#{traefik_name}.middlewares": middlewares
      }.merge(router_tls).compact
    end

    def router_tls # rubocop:disable Metrics/AbcSize
      return {} unless @deployment.production?
      return {} unless @frontend.tls?

      {
        "traefik.http.routers.#{traefik_name}.tls": "true",
        "traefik.http.routers.#{traefik_name}.tls.certresolver": @frontend.tls_cert_resolver.presence,
        "traefik.http.routers.#{traefik_name}.tls.domains[0].main": @frontend.tls_domains.first.presence,
        "traefik.http.routers.#{traefik_name}.tls.domains[0].sans": @frontend.tls_domains[1..]&.join(",").presence,
        "traefik.http.routers.#{traefik_name}.tls.options": @frontend.tls_options? ? "#{@frontend.tls_options}@file" : nil
      }.compact
    end

    def router_rule
      if @deployment.production?
        production_router_rule
      else
        review_router_rule
      end
    end

    def production_router_rule
      if @frontend.router_rule?
        @frontend.router_rule
      elsif @frontend.hosts?
        hosts = @frontend.hosts.to_s.split
        validate_hosts!(hosts)

        "Host(#{hosts.map { |host| "`#{host}`" }.join(',')})"
      else
        review_router_rule
      end
    end

    def review_router_rule
      host = [[@stack.short_name, @frontend.name, @deployment.name].join("-"), Current.settings.host].join(".")
      validate_hosts!(host)

      "Host(`#{host}`)"
    end

    def validate_hosts!(hosts)
      Array.wrap(hosts).each do |host|
        raise DockerComposeService::Error, "The hostname \"#{host}\" is too long (max. 64 characters)" if host.size > 64
        raise DockerComposeService::Error, "The hostname \"#{host}\" is invalid" unless host.match?(HOSTNAME_REGEXP)
      end
    end

    def middlewares # rubocop:disable Metrics/AbcSize
      [].tap do |middlewares|
        middlewares << "#{traefik_name}-baiscauth" if auth_basic.any?
        middlewares << "#{traefik_name}-errors" if errors.any?
        middlewares << "#{traefik_name}-headers" if custom_headers.any?
        middlewares << "#{traefik_name}-hsts" if hsts.any?
        middlewares << "#{traefik_name}-redirect" if redirect.any?
      end.join(",")
    end

    def auth_basic
      return {} unless @frontend.auth_basic?

      {
        "traefik.http.middlewares.#{traefik_name}-baiscauth.basicauth.users": auth_basic_users,
        "traefik.http.middlewares.#{traefik_name}-baiscauth.basicauth.removeheader": @frontend.auth_basic_remove_header.to_s
      }
    end

    def errors
      return {} unless Current.settings.backend_name?

      {
        "traefik.http.middlewares.#{traefik_name}-errors.errors.service": Current.settings.backend_name,
        "traefik.http.middlewares.#{traefik_name}-errors.errors.status": "502,503,504",
        "traefik.http.middlewares.#{traefik_name}-errors.errors.query": "/errors/service_unavailable"
      }
    end

    def redirect
      return {} unless @frontend.redirect?

      {
        "traefik.http.middlewares.#{traefik_name}-redirect.redirectregex.regex": @frontend.redirect_regex,
        "traefik.http.middlewares.#{traefik_name}-redirect.redirectregex.replacement": @frontend.redirect_replacement,
        "traefik.http.middlewares.#{traefik_name}-redirect.redirectregex.permanent": @frontend.redirect_permanent.to_s
      }
    end

    def custom_headers
      headers = {}

      transform_header(@frontend.request_headers) do |key, value|
        headers[:"traefik.http.middlewares.#{traefik_name}-headers.headers.customRequestHeaders.#{key}"] = value
      end

      transform_header(@frontend.response_headers) do |key, value|
        headers[:"traefik.http.middlewares.#{traefik_name}-headers.headers.customResponseHeaders.#{key}"] = value
      end

      headers
    end

    def hsts
      return {} unless @frontend.hsts?
      return {} unless @deployment.production?

      {
        "traefik.http.middlewares.#{traefik_name}-hsts.headers.forceSTSHeader": "true",
        "traefik.http.middlewares.#{traefik_name}-hsts.headers.stsSeconds": @frontend.hsts_max_age,
        "traefik.http.middlewares.#{traefik_name}-hsts.headers.stsIncludeSubdomains": @frontend.hsts_include_subdomain.to_s
      }
    end

    def auth_basic_users
      YAML.safe_load(@frontend.auth_basic_users).map do |key, value|
        "#{key}:#{BCrypt::Password.create value.squish}"
      end.join(",")
    rescue StandardError => exception
      Rails.logger.warn "FrontendAuthBasicUserException: #{exception.class}: #{exception.message}"

      nil
    end

    def transform_header(headers, &block)
      return [] if headers.blank?

      YAML.safe_load(headers).map(&block)
    rescue StandardError => exception
      Rails.logger.warn "FrontendTransformHandersException: #{exception.class}: #{exception.message}"

      []
    end
  end
end
