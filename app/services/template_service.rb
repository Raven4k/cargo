# frozen_string_literal: true

class TemplateService
  include ActiveModel::Model

  attr_reader :name

  validate :validate_input_presence

  class << self
    def all
      Dir.glob(Rails.root.join("db/templates/*.yml")).map do |file|
        new(name: File.basename(file, ".yml"), stack: nil)
      end
    end
  end

  def initialize(name:, stack:)
    @file = Rails.root.join("db", "templates", "#{name}.yml")
    raise ArgumentError, "name is invalid" unless @file.exist?

    @name = name
    @stack = stack
    @data = OpenStruct.new
  end

  def method_missing(name, *args, &block)
    if inputs.key?(name.to_s.delete("="))
      @data.public_send(name, *args)
    else
      super
    end
  end

  def respond_to_missing?(name, include_private = false)
    return true if inputs.key?(name.to_s.delete("="))

    super
  end

  def inputs
    @inputs ||= template.fetch("inputs", {}).map do |name, value|
      options = {
        input_html: { value: @data[name].presence || value.presence }.compact
      }.compact

      [name, options]
    end.compact.to_h
  end

  def perform(attributes = {})
    assign_attributes attributes
    return false unless valid?

    return true if Template::ImportService.new(file: @file, stack: @stack, inputs: @data).import

    errors.add(:base, "Template import failed")
    false
  end

private

  def template
    @template ||= YAML.load_file(@file)
  end

  def validate_input_presence
    inputs.each_key do |name|
      errors.add(name, :blank) if @data[name].blank?
    end
  end
end
