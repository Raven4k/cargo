# frozen_string_literal: true

class DockerComposeService
  class Error < StandardError; end

  def initialize(deployment:)
    @deployment = deployment

    @services = @deployment.stack.services.map { |service| DockerCompose::ServiceService.new(service: service, deployment: @deployment) }
  end

  def content
    {
      version: "3.7",
      services: services,
      configs: configs,
      secrets: secrets,
      volumes: volumes,
      networks: networks
    }.deep_stringify_keys.compact
  end

private

  def services
    @services.map(&:service).inject(:merge).presence
  end

  def configs
    @services.map(&:configs).flatten.map(&:stack_config).compact.inject(:merge).presence
  end

  def secrets
    @services.map(&:configs).flatten.map(&:stack_secret).compact.inject(:merge).presence
  end

  def volumes
    @services.map(&:volumes).flatten.map(&:stack_claim).inject(:merge).presence
  end

  def networks
    {
      Current.settings.traefik_network => {
        external: true
      },
      overlay: {
        driver: "overlay",
        driver_opts: { encrypted: "true" }
      }
    }
  end
end
