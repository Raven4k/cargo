# frozen_string_literal: true

class EnvironmentPolicy < ApplicationPolicy
  def show?
    return false unless record.persisted?
    return true if user.admin?

    user.stack_ids.include?(record.stack_id)
  end

  def logs?
    show?
  end

  def terminal?
    show?
  end

  def restart_service?
    show?
  end

  def metrics?
    return false unless Current.settings.prometheus?

    show?
  end

  def create?
    true
  end

  def destroy?
    show? && !record.production?
  end
end
