# frozen_string_literal: true

class StackPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    return false unless record.persisted?
    return true if user.admin?

    user.stack_ids.include?(record.id)
  end

  def create?
    user.admin? || Current.settings.allow_creation_of_stacks
  end

  def update?
    show?
  end

  def template?
    update? && record.fresh?
  end

  def destroy?
    return false unless record.persisted?
    return true if user.admin?

    Current.settings.allow_creation_of_stacks && user.stack_ids.include?(record.id)
  end
end
