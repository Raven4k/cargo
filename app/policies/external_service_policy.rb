# frozen_string_literal: true

class ExternalServicePolicy < ApplicationPolicy
  def index?
    Current.settings.external_services?
  end

  def show?
    return false unless Current.settings.external_services?
    return false unless record.persisted?
    return true if user.admin?

    user.stack_ids.include?(record.stack_id)
  end

  def create?
    Current.settings.external_services?
  end

  def update?
    return false unless Current.settings.external_services?
    return true if user.admin?

    user.stack_ids.include?(record.stack_id)
  end

  def destroy?
    show?
  end
end
