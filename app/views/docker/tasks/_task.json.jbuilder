# frozen_string_literal: true

json.id task.id
json.service_id task.service_id
json.image task.image
json.desired_state task.desired_state
json.state task.state
json.error task.error
json.created_at task.created_at
