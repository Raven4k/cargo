# frozen_string_literal: true

json.name @environment.name

json.docker_services @environment.docker_services, partial: "docker/services/service", as: :service
