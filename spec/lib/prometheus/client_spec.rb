# frozen_string_literal: true

require "rails_helper"

RSpec.describe Prometheus::Client do
  subject(:client) { described_class.new }

  describe ".query_range" do
    before { expect(client).to receive(:agent).and_return(agent) }

    let(:agent) { instance_double("Faraday::Connection", get: true) }

    it "includes default attributes" do
      expect(client.query_range({})).to be_truthy
      expect(agent).to have_received(:get).with("/api/v1/query_range", hash_including(timeout: 10, step: 60))
    end

    it "includes parsed start" do
      expect(client.query_range(start: "2019-01-01 10:00:00".to_datetime)).to be_truthy
      expect(agent).to have_received(:get).with("/api/v1/query_range", hash_including(start: "2019-01-01T10:00:00+00:00"))
    end

    it "includes parsed end" do
      expect(client.query_range(end: "2019-01-01 12:00:00".to_datetime)).to be_truthy
      expect(agent).to have_received(:get).with("/api/v1/query_range", hash_including(end: "2019-01-01T12:00:00+00:00"))
    end

    it "includes query" do
      expect(client.query_range(query: "Query")).to be_truthy
      expect(agent).to have_received(:get).with("/api/v1/query_range", hash_including(query: "Query"))
    end

    it "includes timeout" do
      expect(client.query_range(timeout: 999)).to be_truthy
      expect(agent).to have_received(:get).with("/api/v1/query_range", hash_including(timeout: 999))
    end

    it "includes step" do
      expect(client.query_range(step: 999)).to be_truthy
      expect(agent).to have_received(:get).with("/api/v1/query_range", hash_including(step: 999))
    end
  end

  describe ".agent" do
    it do
      expect(Current.settings).to receive(:prometheus?).and_return(true)
      expect(Current.settings).to receive(:prometheus_host).and_return("http://prometheus_host/")

      expect(client.send(:agent)).to be_instance_of(Faraday::Connection)
    end

    it do
      expect(Current.settings).to receive(:prometheus?).and_return(false)
      expect(Current.settings).not_to receive(:prometheus_host)

      expect { client.send(:agent) }.to raise_error(RuntimeError, "Prometheus config missing")
    end
  end
end
