# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCli::Service do
  let(:name) { "Name" }

  describe ".update" do
    it { expect(described_class.update(name: name)).to eq("stdout\nstderr") }

    it do
      described_class.update(name: name)
      expect(Open3).to have_received(:capture3).with("docker", "service", "update", "--force", "--detach", "--with-registry-auth", name.downcase)
    end
  end
end
