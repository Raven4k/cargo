# frozen_string_literal: true

require "rails_helper"

RSpec.describe Template::ImportService do
  subject(:service) { described_class.new(file: file, stack: stack, inputs: inputs) }

  let(:file) { Rails.root.join("spec/support/template.yml") }
  let(:stack) { create(:stack) }
  let(:inputs) { OpenStruct.new(image: "nginx:lastest") }

  describe "#import" do
    it "is successfully" do # rubocop:disable RSpec/MultipleExpectations
      expect(service.import).to be_truthy
      expect(Service.count).to be(1)
      expect(Config.count).to be(2)
      expect(ConfigClaim.count).to be(1)
      expect(Volume.count).to be(1)
      expect(VolumeClaim.count).to be(1)
      expect(Frontend.count).to be(1)
    end

    it "has diffrent config values" do
      expect(service.import).to be_truthy
      expect(Config.find_by(name: "test2").review_value).to eq("Review")
      expect(Config.find_by(name: "test2").value).to eq("Production")
      expect(Config.find_by(name: "test1").review_value).to eq(Config.find_by(name: "test1").value)
    end

    it "returns false if an error occours" do
      expect(stack.services).to receive(:create!).and_raise(ActiveRecord::RecordInvalid)
      expect(service.import).to be_falsey
      expect(stack.reload).to be_fresh
    end

    context "when stack is not fresh" do
      before { create(:service, stack: stack) }

      it { expect(service.import).to be_falsey }
    end
  end
end
