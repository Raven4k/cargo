# frozen_string_literal: true

require "rails_helper"

RSpec.describe MetricsService, type: :service do
  subject(:metrics) { described_class.new(environment, service, end: "2019-01-01 10:00:00".to_datetime) }

  let(:environment) { create(:environment) }
  let(:service) { create(:service) }

  let(:prometheus) { instance_double("Prometheus::Client") }

  before { allow(Prometheus::Client).to receive(:new).and_return(prometheus) }

  context "when traefik version is v1" do
    before { allow(Current.settings).to receive(:traefik_version).and_return("v1") }

    describe "#requests" do
      it "queries prometheus" do
        expect(prometheus).to receive(:query_range).with(hash_including(query: /traefik_backend_requests_total/)).and_return(instance_double("Faraday::Response", body: { status: "success" }))

        expect(metrics.requests).to eq(status: "success")
      end
    end

    describe "#response_time" do
      it "queries prometheus" do
        expect(prometheus).to receive(:query_range).with(hash_including(query: /traefik_backend_request_duration_seconds_sum/)).and_return(instance_double("Faraday::Response", body: { status: "success" }))

        expect(metrics.response_time).to eq(status: "success")
      end
    end
  end

  context "when traefik version is v2" do
    before { allow(Current.settings).to receive(:traefik_version).and_return("v2") }

    describe "#requests" do
      it "queries prometheus" do
        expect(prometheus).to receive(:query_range).with(hash_including(query: /traefik_service_requests_total/)).and_return(instance_double("Faraday::Response", body: { status: "success" }))

        expect(metrics.requests).to eq(status: "success")
      end
    end

    describe "#response_time" do
      it "queries prometheus" do
        expect(prometheus).to receive(:query_range).with(hash_including(query: /traefik_service_request_duration_seconds_sum/)).and_return(instance_double("Faraday::Response", body: { status: "success" }))

        expect(metrics.response_time).to eq(status: "success")
      end
    end
  end
end
