# frozen_string_literal: true

require "rails_helper"

RSpec.describe LogsStreamService, type: :service do
  subject { described_class.new(service: service, stream: stream, params: params) }

  let(:stack) { DockerApi::Stack.new(name: "cargo_test_test") }
  let(:service) { stack.services.first }
  let(:stream) { double(write: true, close: true) }
  let(:params) { { since: since } }
  let(:since) { "2019-01-01T10:00:00Z" }

  describe "#run" do
    after do
      expect(stream).to have_received(:close)
      expect(stream).to have_received(:write).at_least(:once)
    end

    let(:result) { subject.run }

    it { expect(result).to be_nil }

    context "when since is an invalid date" do
      let(:since) { "60-60-60" }

      it { expect(result).to be_nil }
    end
  end
end
