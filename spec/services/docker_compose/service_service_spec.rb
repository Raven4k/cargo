# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCompose::ServiceService do
  subject { described_class.new(service: service, deployment: deployment) }

  let(:service) { build(:service, frontends: [build(:frontend)]) }
  let(:deployment) { build(:deployment) }

  describe "#service" do
    let(:result) { subject.service }
    let(:options) { result[service.name.downcase] }

    it { expect(result).to be_a(Hash) }
    it { expect(result).to have_key(service.name.downcase) }

    describe "options" do
      it { expect(options).to be_a(Hash) }
      it { expect(options).to be_hash_with("image", service.image) }

      describe "#image" do
        let(:service) { build(:service, image: "test") }

        it { expect(options).to be_hash_with("image", "test:latest") }

        context "with tags" do
          let(:deployment) { build(:deployment, tags: "#{service.name.downcase}: 1244") }

          it { expect(options).to be_hash_with("image", "test:1244") }
        end

        context "with images" do
          let(:deployment) { build(:deployment, images: "#{service.name.downcase}: registry/repo:tag") }

          it { expect(options).to be_hash_with("image", "registry/repo:tag") }
        end
      end

      describe "healthcheck" do
        let(:service) { build(:service, healthcheck: true, frontends: [build(:frontend)]) }
        let(:healthcheck) { options[:healthcheck] }

        it { expect(healthcheck[:test]).to be_a(Array) }
        it { expect(healthcheck[:test].last).to eq("curl -f http://localhost || exit 1") }
        it { expect(healthcheck).to be_hash_with("interval", "30s") }
        it { expect(healthcheck).to be_hash_with("timeout", "30s") }
        it { expect(healthcheck).to be_hash_with("retries", 3) }
        it { expect(healthcheck).to be_hash_with("start_period", "0s") }
      end

      describe "placement" do
        let(:placement) { options.dig(:deploy, :placement) }

        it { expect(placement).to be_nil }
        it { expect(deployment.environment.node_label).to be_blank }

        context "when node_label is defined" do
          before { deployment.environment.node_label = "label" }

          it { expect(placement).to be_a(Hash) }
          it { expect(placement[:constraints]).to eq(["node.labels.cargo_label == true"]) }
        end
      end
    end
  end
end
