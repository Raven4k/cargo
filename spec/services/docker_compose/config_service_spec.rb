# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCompose::ConfigService do
  subject { described_class.new(config_claim: config_claim, deployment: deployment) }

  let(:config) { build(:config) }
  let(:config_claim) { build(:config_claim, config: config) }
  let(:environment) { create(:environment, name: "production") }
  let(:deployment) { create(:deployment, environment: environment) }

  describe "#config" do
    let(:result) { subject.config }

    context "when not a config" do
      let(:config) { build(:config_environment) }

      it { expect(result).to be_nil }
    end

    context "when config" do
      let(:config) { build(:config) }

      it { expect(result).to be_a(Hash) }
      it { expect(result[:source]).to eq(config.docker_name) }
      it { expect(result[:target]).to eq(config_claim.target) }
    end
  end

  describe "#environment" do
    let(:result) { subject.environment }

    context "when not an environment" do
      let(:config) { build(:config) }

      it { expect(result).to be_nil }
    end

    context "when an environment" do
      let(:config) { build(:config_environment) }

      it { expect(result).to be_a(Hash) }
      it { expect(result[config_claim.target]).to eq(config.formatted_value) }
    end
  end

  describe "#secret" do
    let(:result) { subject.secret }

    context "when not a secret" do
      let(:config) { build(:config) }

      it { expect(result).to be_nil }
    end

    context "when a secret" do
      let(:config) { build(:config_secret) }

      it { expect(result).to be_a(Hash) }
      it { expect(result[:source]).to eq(config.docker_name) }
      it { expect(result[:target]).to eq(config_claim.target) }
    end
  end

  describe "#stack_config" do
    let(:result) { subject.stack_config }

    context "when not a config" do
      let(:config) { build(:config_environment) }

      it { expect(result).to be_nil }
    end

    context "when a config" do
      let(:config) { build(:config) }

      it { expect(result).to be_a(Hash) }
      it { expect(result[config.docker_name]).to eq(external: true) }
    end
  end

  describe "#stack_secret" do
    let(:result) { subject.stack_secret }

    context "when not a secret" do
      let(:config) { build(:config) }

      it { expect(result).to be_nil }
    end

    context "when a secret" do
      let(:config) { build(:config_secret) }

      it { expect(result).to be_a(Hash) }
      it { expect(result[config.docker_name]).to eq(external: true) }
    end
  end
end
