# frozen_string_literal: true

require "rails_helper"

RSpec.describe TerminalSessionService do
  subject(:session) { described_class.new(container_id, channel) }

  let(:container_id) { "b0e1ddbcc140b88f485f13e4e1d594fc9606195e2573a6dba1b1f80882074d7c" }
  let(:channel) { instance_double("Channel", reject_subscription: true) }
  let(:shell) { "/bin/bash" }

  let(:exec_instance) { instance_double("Docker::Exec", id: "id", resize: true) }
  let(:container) { instance_double("Docker::Container", id: container_id, exec: [[shell]], connection: "connection") }

  before do
    allow(Docker::Exec).to receive(:create).with(hash_including("Container" => container_id, "Cmd" => array_including(shell)), "connection").and_return(exec_instance)
    allow(Docker::Container).to receive(:get).with(container_id).and_return(container)
  end

  describe "#start" do
    it "starts the exec instance" do
      expect(Thread).to receive(:start).and_yield
      expect(session).to receive(:start_exec).and_yield(instance_double("Socket", readpartial: nil))

      session.start

      expect(channel).to have_received(:reject_subscription)
    end

    it "catches timout error" do
      expect(Thread).to receive(:start).and_yield
      expect(session).to receive(:start_exec).and_raise(Docker::Error::TimeoutError)

      session.start

      expect(channel).to have_received(:reject_subscription)
    end
  end

  describe "#stream" do
    it { expect(session.stream).to match(/^session_(\w+)$/) }
  end

  describe "#write" do
    it "writes to the socket" do
      socket = instance_double("Socket", closed?: false, write: true)
      session.instance_variable_set("@socket", socket)

      expect(session.write("command")).to be_truthy
      expect(socket).to have_received(:write).with("command")
    end

    it "doesn't write to the socket if the socket is closed" do
      socket = instance_double("Socket", closed?: true, write: true)
      session.instance_variable_set("@socket", socket)

      expect(session.write("command")).to be_nil
      expect(socket).not_to have_received(:write)
    end
  end

  describe "#resize" do
    it "resizes exec instance" do
      expect(session.resize(cols: 2, rows: 4)).to be_truthy
      expect(exec_instance).to have_received(:resize).with(h: 4, w: 2)
    end
  end

  describe "#close" do
    it "closes the socket" do
      socket = instance_double("Socket", closed?: false, close_write: true)
      session.instance_variable_set("@socket", socket)

      expect(session.close).to be_truthy
      expect(socket).to have_received(:close_write)
    end

    it "doesn't close the socket" do
      socket = instance_double("Socket", closed?: true, close_write: true)
      session.instance_variable_set("@socket", socket)

      expect(session.close).to be_nil
      expect(socket).not_to have_received(:close_write)
    end
  end

  describe "#shell" do
    let(:shell) { "sh" }

    it("fallsback to sh") do
      allow(container).to receive(:exec).and_return([["something"]])
      expect(session.send(:shell)).to eq("sh")
    end
  end

  describe "#start_exec" do
    it do
      expect(Docker.connection).to receive(:post).with("/exec/id/start", any_args)
      session.send(:start_exec) { nil }
    end
  end

  describe "#read_socket" do
    let(:socket) { instance_double("Socket", readpartial: true) }

    it do
      expect(SecureRandom).to receive(:hex).and_return("random")
      expect(socket).to receive(:readpartial).and_return(+"chuck", nil)
      expect(ActionCable.server).to receive(:broadcast).with("session_random", c: "chuck")

      expect(session.send(:read_socket, socket)).to be_nil
    end

    it do
      expect(socket).to receive(:readpartial).and_raise(IOError)

      expect(session.send(:read_socket, socket)).to be_nil
    end
  end
end
