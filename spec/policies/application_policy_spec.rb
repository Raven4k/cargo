# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationPolicy, type: :policy do
  subject { described_class }

  permissions ".scope" do
    it { expect(described_class::Scope.new(:any_user, Stack).resolve).to eq(Stack.all) }
  end

  permissions :index? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end

  permissions :show? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end

  permissions :new? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end

  permissions :create? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end

  permissions :edit? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end

  permissions :update? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end

  permissions :destroy? do
    it { is_expected.not_to permit(:any_user, :any_instance) }
  end
end
