# frozen_string_literal: true

require "rails_helper"

RSpec.describe VolumePolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:volume) { create(:volume) }
  let(:related_volume) { create(:volume, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :index? do
    it { is_expected.to permit(user, Volume) }
  end

  permissions :show? do
    it { is_expected.not_to permit(user, volume) }
    it { is_expected.to permit(user, related_volume) }

    it { is_expected.to permit(admin, volume) }
    it { is_expected.to permit(admin, related_volume) }
    it { is_expected.not_to permit(admin, Volume.new) }
  end

  permissions :new?, :create? do
    it { is_expected.to permit(user, Volume) }
  end

  permissions :edit?, :update? do
    it { is_expected.not_to permit(user, volume) }
    it { is_expected.to permit(user, related_volume) }

    it { is_expected.to permit(admin, volume) }
    it { is_expected.to permit(admin, related_volume) }
  end

  permissions :destroy? do
    it { is_expected.not_to permit(user, volume) }
    it { is_expected.to permit(user, related_volume) }

    it { is_expected.to permit(admin, volume) }
    it { is_expected.to permit(admin, related_volume) }
  end
end
