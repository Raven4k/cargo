# frozen_string_literal: true

require "rails_helper"

RSpec.describe FrontendPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:frontend) { create(:frontend) }
  let(:related_frontend) { create(:frontend, stack: stack, service: service) }
  let(:service) { create(:service, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :index? do
    it { is_expected.to permit(user, Frontend) }
  end

  permissions :show? do
    it { is_expected.not_to permit(user, frontend) }
    it { is_expected.to permit(user, related_frontend) }
    it { is_expected.not_to permit(user, Frontend.new) }

    it { is_expected.to permit(admin, frontend) }
    it { is_expected.to permit(admin, related_frontend) }
    it { is_expected.not_to permit(admin, Frontend.new) }
  end

  permissions :new?, :create? do
    it { is_expected.to permit(user, Frontend) }
  end

  permissions :edit?, :update? do
    it { is_expected.not_to permit(user, frontend) }
    it { is_expected.to permit(user, related_frontend) }

    it { is_expected.to permit(admin, frontend) }
    it { is_expected.to permit(admin, related_frontend) }
  end

  permissions :destroy? do
    it { is_expected.not_to permit(user, frontend) }
    it { is_expected.to permit(user, related_frontend) }

    it { is_expected.to permit(admin, frontend) }
    it { is_expected.to permit(admin, related_frontend) }
  end
end
