# frozen_string_literal: true

require "rails_helper"

RSpec.describe DeploymentPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:deployment) { create(:deployment) }
  let(:related_deployment) { create(:deployment, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :index? do
    it { is_expected.to permit(user, Deployment) }
  end

  permissions :show? do
    it { is_expected.not_to permit(user, deployment) }
    it { is_expected.to permit(user, related_deployment) }

    it { is_expected.to permit(admin, deployment) }
    it { is_expected.to permit(admin, related_deployment) }
    it { is_expected.not_to permit(admin, Deployment.new) }
  end

  permissions :new?, :create? do
    it { is_expected.to permit(user, Deployment) }
  end
end
