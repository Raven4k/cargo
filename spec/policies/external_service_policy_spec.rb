# frozen_string_literal: true

require "rails_helper"

RSpec.describe ExternalServicePolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:external_service) { create(:external_service) }
  let(:related_external_service) { create(:external_service, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  context "when external_services is enabled" do
    before { allow(Current.settings).to receive(:external_services?).and_return(true) }

    permissions :index? do
      it { is_expected.to permit(user, ExternalService) }
    end

    permissions :show? do
      it { is_expected.not_to permit(user, external_service) }
      it { is_expected.to permit(user, related_external_service) }

      it { is_expected.to permit(admin, external_service) }
      it { is_expected.to permit(admin, related_external_service) }
      it { is_expected.not_to permit(admin, ExternalService.new) }
    end

    permissions :new?, :create? do
      it { is_expected.to permit(user, ExternalService) }
    end

    permissions :edit?, :update? do
      it { is_expected.not_to permit(user, external_service) }
      it { is_expected.to permit(user, related_external_service) }

      it { is_expected.to permit(admin, external_service) }
      it { is_expected.to permit(admin, related_external_service) }
    end

    permissions :destroy? do
      it { is_expected.not_to permit(user, external_service) }
      it { is_expected.to permit(user, related_external_service) }

      it { is_expected.to permit(admin, external_service) }
      it { is_expected.to permit(admin, related_external_service) }
    end
  end

  context "when external_services is disabled" do
    before { allow(Current.settings).to receive(:external_services?).and_return(false) }

    permissions :index?, :new?, :create?, :edit?, :update?,  :destroy? do
      it { is_expected.not_to permit(user, external_service) }
      it { is_expected.not_to permit(user, related_external_service) }

      it { is_expected.not_to permit(admin, external_service) }
      it { is_expected.not_to permit(admin, related_external_service) }
    end
  end
end
