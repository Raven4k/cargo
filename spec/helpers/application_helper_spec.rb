# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "#alert_style_class" do
    it { expect(helper.alert_style_class("notice")).to eq("alert-info") }
    it { expect(helper.alert_style_class(:success)).to eq("alert-success") }
    it { expect(helper.alert_style_class("unknown")).to eq("alert-danger") }
    it { expect(helper.alert_style_class("alert")).to eq("alert-danger") }
  end
end
