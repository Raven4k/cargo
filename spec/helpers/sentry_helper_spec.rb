# frozen_string_literal: true

require "rails_helper"

RSpec.describe SentryHelper, type: :helper do
  helper { def current_user; end }

  describe "#sentry_meta_tag" do
    context "when capture is allowed" do
      before { allow(Raven.configuration).to receive(:capture_allowed?).and_return(true) }

      it { expect(helper.sentry_meta_tag).to include("meta") }
      it { expect(helper.sentry_meta_tag).to include("sentry") }

      it do
        expect(helper).to receive(:sentry_options).and_return(test: :test)
        expect(helper.sentry_meta_tag).to include("eyJ0ZXN0IjoidGVzdCJ9")
      end

      it { expect(helper.send(:sentry_options)[:user]).to be_nil }

      context "when user is signed in" do
        before { allow(helper).to receive(:current_user).and_return(user) }

        let(:user) { create(:user) }

        it { expect(helper.sentry_meta_tag).to include("sentry") }
        it { expect(helper.send(:sentry_options)[:user]).to be_a(Hash) }
        it { expect(helper.send(:sentry_options)[:user][:id]).to eq(user.id) }
        it { expect(helper.send(:sentry_options)[:user][:username]).to eq(user.username) }
      end
    end

    context "when capture is not allowed" do
      it { expect(helper.sentry_meta_tag).to be_nil }
    end
  end
end
