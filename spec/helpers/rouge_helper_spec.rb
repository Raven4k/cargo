# frozen_string_literal: true

require "rails_helper"

RSpec.describe RougeHelper, type: :helper do
  describe "#rouge" do
    it { expect(helper.rouge("test: test", language: :yaml)).to eq("<pre class=\"highlight mb-0\"><div class=\"line-1\"><span class=\"na\">test</span><span class=\"pi\">:</span> <span class=\"s\">test</span>\n</div></pre>") }
  end
end
