# frozen_string_literal: true

require "rails_helper"

RSpec.describe DeploymentsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let!(:environment) { create(:environment, stack: stack) }

  describe "#index" do
    it "contains the deployment" do
      deployment = create(:deployment, stack: stack, environment: environment)
      get stack_deployments_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(deployment.version)
      expect(response.body).to include(deployment_path(deployment))
    end

    it "contains the link to create a new deployment" do
      get stack_deployments_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_deployment_path(stack))
    end
  end

  describe "#show" do
    let(:deployment) { create(:deployment, stack: stack, environment: environment) }

    it "shows the deployment" do
      get deployment_path(deployment)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(deployment.name)
      expect(response.body).to include(deployment.version)
    end
  end

  describe "#new" do
    it "shows the form to create a deployment" do
      get new_stack_deployment_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New deployment")
      expect(response.body).to include(%(name="deployment[version]"))
      expect(response.body).to include("Create Deployment")
    end

    it "shows a prefilled form to create a deployment" do
      environment.deployments.create(version: "prefilled_version")
      get new_stack_deployment_path(stack, environment: environment.name)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New deployment")
      expect(response.body).to include("prefilled_version")
    end
  end

  describe "#create" do
    it "creates a deployment" do
      expect do
        post stack_deployments_path(stack), params: { deployment: { environment_id: environment.id } }
      end.to change(Deployment, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_deployments_path(stack))
    end

    it "renders validation errors" do
      post stack_deployments_path(stack), params: { deployment: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Environment must exist")
    end
  end
end
