# frozen_string_literal: true

require "rails_helper"

RSpec.describe VolumesController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }

  describe "#index" do
    it "contains the volume" do
      volume = create(:volume, stack: stack)

      get stack_volumes_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(volume.name)
      expect(response.body).to include(volume_path(volume))
    end

    it "contains the link to create a new volume" do
      get stack_volumes_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_volume_path(stack))
    end
  end

  describe "#show" do
    let(:volume) { create(:volume, stack: stack) }

    it "shows the volume" do
      get volume_path(volume)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(volume.name)
    end

    it "contains the link to edit the volume" do
      get volume_path(volume)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(edit_volume_path(volume))
    end
  end

  describe "#new" do
    it "shows the form to create a volume" do
      get new_stack_volume_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New volume")
      expect(response.body).to include(%(name="volume[name]"))
      expect(response.body).to include("Create Volume")
    end
  end

  describe "#create" do
    it "creates a volume" do
      expect do
        post stack_volumes_path(stack), params: { volume: { name: "Test" } }
      end.to change(Volume, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{volumes/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stack_volumes_path(stack), params: { volume: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#edit" do
    let(:volume) { create(:volume, stack: stack) }

    it "shows the form to create a volume" do
      get edit_volume_path(volume)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing volume")
      expect(response.body).to include(%(name="volume[name]"))
      expect(response.body).to include("Update Volume")
    end
  end

  describe "#update" do
    let(:volume) { create(:volume, stack: stack) }

    it "updates the volume" do
      put volume_path(volume), params: { volume: { name: "Test" } }

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(volume_path(volume))
    end

    it "renders validation errors" do
      put volume_path(volume), params: { volume: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#destroy" do
    let(:volume) { create(:volume, stack: stack) }

    it "destroys the volume" do
      delete volume_path(volume)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_volumes_path(stack))
    end

    it "does not destroy the volume" do
      expect_any_instance_of(Volume).to receive(:destroy).and_return(false)

      delete volume_path(volume)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(volume_path(volume))
    end
  end
end
