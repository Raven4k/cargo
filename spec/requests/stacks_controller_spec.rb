# frozen_string_literal: true

require "rails_helper"

RSpec.describe StacksController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }

  describe "#index" do
    it "contains the stack" do
      stack = create(:stack, users: [user])

      get stacks_path

      expect(response).to have_http_status(:success)
      expect(response.body).to include(stack.name)
      expect(response.body).to include(stack_path(stack))
    end

    it "contains the link to create a new stack" do
      get stacks_path

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_path)
    end
  end

  describe "#show" do
    let(:stack) { create(:stack, users: [user]) }

    it "shows the stack" do
      environment = create(:environment, stack: stack, last_deployment: create(:deployment))

      get stack_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(stack.name)
      expect(response.body).to include(environment.name)
    end

    it "contains the link to edit the stack" do
      get stack_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(edit_stack_path(stack))
    end
  end

  describe "#new" do
    it "shows the form to create a stack" do
      get new_stack_path

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New stack")
      expect(response.body).to include(%(name="stack[name]"))
      expect(response.body).to include("Create Stack")
    end
  end

  describe "#create" do
    it "creates a stack" do
      expect do
        post stacks_path, params: { stack: { name: "Test", short_name: "t" } }
      end.to change(Stack, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{stacks/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stacks_path, params: { stack: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#edit" do
    let(:stack) { create(:stack, users: [user]) }

    it "shows the form to create a stack" do
      get edit_stack_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing stack")
      expect(response.body).to include(%(name="stack[name]"))
      expect(response.body).to include("Update Stack")
    end
  end

  describe "#update" do
    let(:stack) { create(:stack, users: [user]) }

    it "updates the stack" do
      expect do
        put stack_path(stack), params: { stack: { registry_url: "https://example.com" } }
      end.to change { stack.reload.registry_url }.from(stack.registry_url).to("https://example.com")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_path(stack))
    end

    it "renders validation errors" do
      put stack_path(stack), params: { stack: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#destroy" do
    let(:stack) { create(:stack, users: [user]) }

    it "destroys the stack" do
      delete stack_path(stack)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stacks_path)
    end

    it "does not destroy the stack" do
      expect_any_instance_of(Stack).to receive(:destroy).and_return(false)

      delete stack_path(stack)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_path(stack))
    end
  end
end
