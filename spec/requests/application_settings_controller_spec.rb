# frozen_string_literal: true

require "rails_helper"

RSpec.describe ApplicationSettingsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(current_user) }

  let(:current_user) { create(:admin) }

  describe "#show" do
    it "shows the form to update the application setting" do
      get application_setting_path

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing settings")
      expect(response.body).to include(%(name="application_setting[default_production_environment_name]"))
      expect(response.body).to include("Update Settings")
    end

    context "when current_user is no admin" do
      let(:current_user) { create(:user) }

      it "redirects to root_path" do
        get application_setting_path

        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
        expect(flash[:alert]).to include("not authorized")
      end
    end
  end

  describe "#update" do
    let!(:application_setting) { create(:application_setting) }

    it "updates the application setting" do
      expect do
        put application_setting_path, params: { application_setting: { default_production_environment_name: "test" } }
      end.to change { application_setting.reload.default_production_environment_name }.from("production").to("test")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(application_setting_path)
    end

    it "renders validation errors" do
      put application_setting_path, params: { application_setting: { default_production_environment_name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("can&#39;t be blank")
    end
  end
end
