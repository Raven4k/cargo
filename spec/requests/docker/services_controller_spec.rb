# frozen_string_literal: true

require "rails_helper"

RSpec.describe Docker::ServicesController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:environment) { create(:environment, stack: stack) }

  describe "#restart" do
    it "restart service" do
      post restart_environment_service_path(environment, "304waajn57x2ddae73hlnpt6u")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(environment_path(environment))
      expect(flash[:notice]).to include("Restart")
    end
  end
end
