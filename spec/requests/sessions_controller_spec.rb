# frozen_string_literal: true

require "rails_helper"

RSpec.describe SessionsController, type: :request do
  describe "#create" do
    context "when user is approved" do
      before { allow_any_instance_of(User).to receive(:approved?).and_return(true) }

      it "redirects to root" do
        OmniAuth.config.add_mock(:openid_connect, uid: 1, info: { nickname: "test" }, credentials: { token: "token" })

        get sessions_create_path(provider: :openid_connect)

        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(root_path)
      end
    end

    context "when user is not approved" do
      before { allow_any_instance_of(User).to receive(:approved?).and_return(false) }

      it "redirects to failure" do
        OmniAuth.config.add_mock(:openid_connect, uid: 1, info: { nickname: "test" }, credentials: { token: "token" })

        get sessions_create_path(provider: :openid_connect)

        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(sessions_failure_path)
      end
    end

    it "redirects to failure" do
      OmniAuth.config.add_mock(:openid_connect, uid: 1, info: { nickname: nil }, credentials: { token: "token" })

      get sessions_create_path(provider: :openid_connect)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(sessions_failure_path)
    end

    context "with invalid_credentials" do
      it "redirects to failure" do
        OmniAuth.config.mock_auth[:openid_connect] = :invalid_credentials

        get sessions_create_path(provider: :openid_connect)

        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(sessions_failure_path(message: :invalid_credentials, strategy: :openid_connect))
      end
    end
  end

  describe "#failure" do
    it "dispalys message" do
      get sessions_failure_path(message: :invalid_credentials, strategy: :openid_connect)
      expect(response.body).to include("invalid_credentials")
    end
  end
end
