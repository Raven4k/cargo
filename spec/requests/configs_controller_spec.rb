# frozen_string_literal: true

require "rails_helper"

RSpec.describe ConfigsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }

  describe "#index" do
    it "contains the config" do
      config = create(:config, stack: stack)

      get stack_configs_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(config.name)
      expect(response.body).to include(config_path(config))
    end

    it "contains the link to create a new config" do
      get stack_configs_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_config_path(stack))
    end
  end

  describe "#show" do
    let(:config) { create(:config, stack: stack) }

    it "shows the config" do
      get config_path(config)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(config.name)
    end

    it "contains the link to edit the config" do
      get config_path(config)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(edit_config_path(config))
    end
  end

  describe "#new" do
    it "shows the form to create a config" do
      get new_stack_config_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New config")
      expect(response.body).to include(%(name="config[name]"))
      expect(response.body).to include("Create Config")
    end
  end

  describe "#create" do
    it "creates a config" do
      expect do
        post stack_configs_path(stack), params: { config: { name: "Test", value: "Value" } }
      end.to change(Config, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{configs/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stack_configs_path(stack), params: { config: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#edit" do
    let(:config) { create(:config, stack: stack) }

    it "shows the form to update a config" do
      get edit_config_path(config)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing config")
      expect(response.body).to include(%(name="config[name]"))
      expect(response.body).to include("Update Config")
    end
  end

  describe "#update" do
    let(:config) { create(:config, stack: stack) }

    it "updates the config" do
      expect do
        put config_path(config), params: { config: { value: "Test" } }
      end.to change { config.reload.value }.from(config.value).to("Test")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(config_path(config))
    end

    it "renders validation errors" do
      put config_path(config), params: { config: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end

    it "returns conflict" do
      put config_path(config), params: { config: { value: "Test", lock_version: 999 } }

      expect(response).to have_http_status(:conflict)
      expect(response.body).to include("Another user has made a change to that Config")
    end
  end

  describe "#destroy" do
    let(:config) { create(:config, stack: stack) }

    it "destroys the config" do
      delete config_path(config)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_configs_path(stack))
    end

    it "does not destroy the config" do
      expect_any_instance_of(Config).to receive(:destroy).and_return(false)

      delete config_path(config)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(config_path(config))
    end
  end
end
