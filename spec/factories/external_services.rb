# frozen_string_literal: true

FactoryBot.define do
  factory :external_service do
    association :stack, factory: :stack
    service_type { "minio" }
    sequence(:name) { |n| "Name-#{n}" }
    sequence(:minio_production_bucket_name) { |n| "minio-production-bucket-name-#{n}" }
  end
end
