# frozen_string_literal: true

FactoryBot.define do
  factory :admin, class: :User do
    username { Faker::Internet.unique.username }
    sequence(:oidc_uid) { |n| "fake-uid-#{n}" }
    sequence(:oidc_token) { |n| "fake-token-#{n}" }
    admin { true }
    approved { true }

    factory :user do
      admin { false }

      after(:create) do |user, _|
        user.update(admin: false)
      end
    end
  end
end
