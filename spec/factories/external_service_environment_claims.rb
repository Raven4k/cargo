# frozen_string_literal: true

FactoryBot.define do
  factory :external_service_environment_claim do
    association :external_service, factory: :external_service
    association :environment, factory: :environment
    settings { {} }
  end
end
