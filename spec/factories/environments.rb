# frozen_string_literal: true

FactoryBot.define do
  factory :environment, class: :Environment do
    association :stack, factory: :stack
    sequence(:name) { |n| "Name-#{n}" }
  end
end
