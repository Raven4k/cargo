# frozen_string_literal: true

FactoryBot.define do
  factory :volume_claim do
    association :volume, factory: :volume
    association :service, factory: :service
    sequence(:target) { |n| "/target/t#{n}" }
    read_only { false }
  end
end
