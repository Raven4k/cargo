# frozen_string_literal: true

FactoryBot.define do
  factory :external_service_claim do
    association :external_service, factory: :external_service
    association :service, factory: :service
    sequence(:target) { |n| "TARGET#{n}" }
  end
end
