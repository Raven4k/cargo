# frozen_string_literal: true

FactoryBot.define do
  factory :volume do
    association :stack, factory: :stack
    sequence(:name) { |n| "Name-#{n}" }
  end
end
