# frozen_string_literal: true

FactoryBot.define do
  factory :service do
    association :stack, factory: :stack
    sequence(:name) { |n| "Name-#{n}" }
    image { "#{name}:latest".downcase }

    healthcheck { false }
    healthcheck_test { "curl -f http://localhost || exit 1" }
  end
end
