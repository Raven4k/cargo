# frozen_string_literal: true

require "rails_helper"

RSpec.describe Service, type: :model do
  subject(:service) { create(:service) }

  it { is_expected.to define_enum_for(:restart_condition) }
  it { is_expected.to define_enum_for(:rollback_failure_action) }
  it { is_expected.to define_enum_for(:rollback_order) }
  it { is_expected.to define_enum_for(:update_failure_action) }
  it { is_expected.to define_enum_for(:update_order) }

  it { expect(described_class).to be_attr_encrypted(:healthcheck_test) }

  it { is_expected.to belong_to(:stack).required }
  it { is_expected.to have_many(:frontends).dependent(:destroy) }
  it { is_expected.to have_many(:config_claims).dependent(:destroy) }
  it { is_expected.to have_many(:volume_claims).dependent(:destroy) }
  it { is_expected.to have_many(:external_service_claims).dependent(:destroy) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:stack_id) }
  it { is_expected.to have_db_index(%i[stack_id name]).unique(true) }
  it { is_expected.to allow_value("a-B-c").for(:name) }
  it { is_expected.not_to allow_value("a_B_c").for(:name) }
  it { is_expected.not_to allow_value("@").for(:name) }

  it { is_expected.to validate_presence_of(:image) }
  it { is_expected.to validate_presence_of(:replicas) }
  it { is_expected.to validate_numericality_of(:replicas).only_integer.is_greater_than(0) }
  it { is_expected.to validate_presence_of(:restart_condition) }
  it { is_expected.to validate_presence_of(:restart_delay) }
  it { is_expected.to validate_numericality_of(:restart_delay).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:restart_window) }
  it { is_expected.to validate_numericality_of(:restart_window).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:rollback_parallelism) }
  it { is_expected.to validate_numericality_of(:rollback_parallelism).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:rollback_delay) }
  it { is_expected.to validate_numericality_of(:rollback_delay).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:rollback_failure_action) }
  it { is_expected.to validate_presence_of(:rollback_monitor) }
  it { is_expected.to validate_numericality_of(:rollback_monitor).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:rollback_order) }
  it { is_expected.to validate_presence_of(:update_parallelism) }
  it { is_expected.to validate_numericality_of(:update_parallelism).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:update_delay) }
  it { is_expected.to validate_numericality_of(:update_delay).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:update_failure_action) }
  it { is_expected.to validate_presence_of(:update_monitor) }
  it { is_expected.to validate_numericality_of(:update_monitor).only_integer.is_greater_than_or_equal_to(0) }
  it { is_expected.to validate_presence_of(:update_order) }

  context "when healthcheck is false" do
    subject { build(:service, healthcheck: false) }

    it { is_expected.not_to validate_presence_of(:healthcheck_test) }
    it { is_expected.not_to validate_numericality_of(:healthcheck_interval) }
    it { is_expected.not_to validate_numericality_of(:healthcheck_timeout) }
    it { is_expected.not_to validate_numericality_of(:healthcheck_start_period) }
    it { is_expected.not_to validate_numericality_of(:healthcheck_retries) }
  end

  context "when healthcheck is true" do
    subject { build(:service, healthcheck: true) }

    it { is_expected.to validate_presence_of(:healthcheck_test) }
    it { is_expected.to validate_numericality_of(:healthcheck_interval).only_integer.is_greater_than(0) }
    it { is_expected.to validate_numericality_of(:healthcheck_timeout).only_integer.is_greater_than(0) }
    it { is_expected.to validate_numericality_of(:healthcheck_start_period).only_integer.is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:healthcheck_retries).only_integer.is_greater_than_or_equal_to(0) }
  end

  it { is_expected.to accept_nested_attributes_for(:config_claims).allow_destroy(true) }
  it { is_expected.to accept_nested_attributes_for(:external_service_claims).allow_destroy(true) }
  it { is_expected.to accept_nested_attributes_for(:volume_claims).allow_destroy(true) }

  describe "#duplicate" do
    let(:duplicate) { service.duplicate }

    it { expect(duplicate).to be_a(described_class) }
    it { expect(duplicate.name).to eq("#{service.name}-clone") }
    it { expect(duplicate).to be_valid }
  end
end
