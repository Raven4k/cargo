# frozen_string_literal: true

require "rails_helper"

RSpec.describe Volume, type: :model do
  subject { build(:volume) }

  it { is_expected.to have_readonly_attribute(:name) }

  it { is_expected.to have_many(:volume_claims).dependent(:restrict_with_error) }
  it { is_expected.to belong_to(:stack).required }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:stack_id) }
  it { is_expected.to have_db_index(%i[stack_id name]).unique(true) }
  it { is_expected.to allow_value("a-B-c-1").for(:name) }
  it { is_expected.not_to allow_value("a_B_c-2").for(:name) }
  it { is_expected.not_to allow_value("@").for(:name) }
end
