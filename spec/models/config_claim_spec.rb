# frozen_string_literal: true

require "rails_helper"

RSpec.describe ConfigClaim, type: :model do
  subject(:config_claim) { build(:config_claim) }

  it { is_expected.to belong_to(:service).required }
  it { is_expected.to belong_to(:config).required }

  it { is_expected.to validate_presence_of(:target) }
  it { is_expected.to validate_uniqueness_of(:target).case_insensitive.scoped_to(:service_id) }

  it { is_expected.not_to allow_value("test:test").for(:target) }

  context "when config is environment" do
    subject(:config_claim) { build(:config_claim, config: build(:config_environment)) }

    it { is_expected.not_to allow_value("test/test").for(:target) }
  end

  context "when config is secret" do
    subject(:config_claim) { build(:config_claim, config: build(:config_secret)) }

    it { is_expected.not_to allow_value("test/test").for(:target) }
  end

  # TODO
  # scope :environments, -> { config_type(:environment) }
  # scope :secrets, -> { config_type(:secret) }
  # scope :configs, -> { config_type(:config) }
  # scope :config_type, ->(type) { joins(:config).where(configs: { config_type: type }) }

  it { is_expected.to delegate_method(:environment?).to(:config) }
  it { is_expected.to delegate_method(:secret?).to(:config) }
  it { is_expected.to delegate_method(:config?).to(:config) }

  describe "#stack" do
    context "when config is nil" do
      subject(:config_claim) { build(:config_claim, config: nil) }

      it { expect(config_claim.stack).to be(config_claim.service.stack) }
    end

    context "when service is nil" do
      subject(:config_claim) { build(:config_claim, service: nil) }

      it { expect(config_claim.stack).to be(config_claim.config.stack) }
    end
  end
end
