# frozen_string_literal: true

require "rails_helper"

RSpec.describe Stack, type: :model do
  subject(:stack) { build(:stack) }

  it { is_expected.to have_many(:stack_members).dependent(:destroy) }
  it { is_expected.to have_many(:users).through(:stack_members) }

  it { is_expected.to have_many(:services).dependent(:destroy) }
  it { is_expected.to have_many(:volumes).dependent(:destroy) }
  it { is_expected.to have_many(:frontends).dependent(:destroy) }
  it { is_expected.to have_many(:configs).dependent(:destroy) }
  it { is_expected.to have_many(:environments).dependent(:destroy) }
  it { is_expected.to have_many(:deployments).dependent(:destroy) }
  it { is_expected.to have_many(:external_services).dependent(:destroy) }

  it { expect(described_class).to be_attr_encrypted(:registry_password) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive }
  it { is_expected.to have_db_index(:name).unique(true) }

  it { is_expected.to have_readonly_attribute(:short_name) }
  it { is_expected.to validate_presence_of(:short_name) }
  it { is_expected.to validate_uniqueness_of(:short_name).case_insensitive }
  it { is_expected.to have_db_index(:short_name).unique(true) }
  it { is_expected.to allow_value("a-B-c-1").for(:short_name) }
  it { is_expected.not_to allow_value("a_B_c_2").for(:short_name) }
  it { is_expected.not_to allow_value("@").for(:short_name) }

  it { is_expected.to validate_uniqueness_of(:token).case_insensitive }
  it { is_expected.to have_db_index(:token).unique(true) }

  it { is_expected.to validate_presence_of(:registry_url) }
  it { is_expected.to validate_presence_of(:registry_username) }
  it { is_expected.to validate_presence_of(:registry_password) }

  describe "#docker_name" do
    it { expect(stack.docker_name).to eq("#{Cargo.config.stack_prefix}_#{stack.short_name.downcase}") }
  end

  describe "#config_options" do
    let(:config_options) { subject.config_options }

    it { expect(config_options).to be_a(Hash) }
    it { expect(config_options["missing"]).to eq("%{missing}") } # rubocop:disable Style/FormatStringToken
  end

  describe "#config_review_options" do
    let(:config_review_options) { subject.config_review_options }

    it { expect(config_review_options).to be_a(Hash) }
    it { expect(config_review_options["missing"]).to eq("%{missing}") } # rubocop:disable Style/FormatStringToken
  end

  describe "#registry?" do
    it "returns true" do
      expect(stack).to be_registry
    end

    it "returns false" do
      stack.assign_attributes(registry_url: nil, registry_username: nil, registry_password: "")
      expect(stack).not_to be_registry
    end
  end

  describe "#validate_default_node_lables" do
    before { Current.settings.node_labels = ["production"] }

    it { is_expected.to allow_value("production").for(:default_production_node_label) }
    it { is_expected.to allow_value("production").for(:default_review_node_label) }
    it { is_expected.to allow_value(nil).for(:default_production_node_label) }
    it { is_expected.to allow_value(nil).for(:default_review_node_label) }
    it { is_expected.not_to allow_value("something").for(:default_production_node_label) }
    it { is_expected.not_to allow_value("something").for(:default_review_node_label) }
  end

  describe "#destroy" do
    it "calls remove_stack on all related environments" do
      create(:environment, stack: stack)

      expect_any_instance_of(Environment).to receive(:remove_stack) # rubocop:disable RSpec/AnyInstance

      stack.destroy
    end
  end
end
