# frozen_string_literal: true

require "rails_helper"

RSpec.describe ExternalServiceClaim, type: :model do
  subject(:external_service_claim) { build(:external_service_claim) }

  it { is_expected.to belong_to(:external_service).required }
  it { is_expected.to belong_to(:service).required }

  it { is_expected.to validate_uniqueness_of(:external_service_id).case_insensitive.scoped_to(:service_id) }
  it { is_expected.to validate_uniqueness_of(:service_id).case_insensitive.scoped_to(:external_service_id) }

  it { is_expected.to validate_presence_of(:target) }
  it { is_expected.to validate_uniqueness_of(:target).case_insensitive.scoped_to(:service_id) }

  describe "#stack" do
    context "when external_service is nil" do
      subject(:external_service_claim) { build(:external_service_claim, external_service: nil) }

      it { expect(external_service_claim.stack).to be(external_service_claim.service.stack) }
    end

    context "when service is nil" do
      subject(:external_service_claim) { build(:external_service_claim, service: nil) }

      it { expect(external_service_claim.stack).to be(external_service_claim.external_service.stack) }
    end
  end
end
