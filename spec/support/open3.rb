# frozen_string_literal: true

require "open3"

RSpec.configure do |config|
  config.before do |example|
    next if example.metadata[:skip_open3]

    allow(Open3).to receive(:capture2e).and_return([{ status: "success", error: "" }.to_json, instance_double(Process::Status, success?: true)])
    allow(Open3).to receive(:capture3).and_return(["stdout", "stderr", 0])
  end
end
