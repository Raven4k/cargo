# frozen_string_literal: true

RSpec.configure do |config|
  config.before do
    Current.reset
  end
end
