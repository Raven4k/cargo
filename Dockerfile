ARG BASE=alpine:latest
FROM $BASE as base

RUN mkdir -p /usr/local/bundle /usr/src/app/node_modules /usr/src/app/public/assets /usr/src/app/public/packs

FROM ruby:2.6.8-alpine3.13

RUN apk add --no-cache --update \
  build-base zlib-dev libxml2-dev libxslt-dev tzdata git curl \
  postgresql-dev \
  nodejs yarn \
  docker

COPY --from=minio/mc:latest /usr/bin/mc /usr/bin/mc

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN gem install bundler

ENV NODE_ENV production
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

COPY Gemfile Gemfile.lock  ./
COPY --from=base /usr/local/bundle /usr/local/bundle
RUN bundle config set frozen 'true' no-cache 'true'; bundle install --jobs $(nproc); bundle clean --force

COPY package.json yarn.lock ./
COPY --from=base /usr/src/app/node_modules ./node_modules
RUN yarn install --frozen-lockfile

COPY . ./

COPY --from=base /usr/src/app/public/assets ./public/assets
COPY --from=base /usr/src/app/public/packs ./public/packs
RUN bundle exec rails assets:precompile "assets:clean[2]" SECRET_KEY_BASE=1; rm -rf tmp/* log/*

ARG REVISION=dev
ENV REVISION $REVISION
RUN echo "$REVISION" > REVISION

EXPOSE 3000
ENTRYPOINT ["bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma"]
