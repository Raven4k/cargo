# frozen_string_literal: true

module DockerApi
  class Stack
    attr_reader :name

    def initialize(name)
      @name = name
    end

    def services
      @services ||= find_services
    end

    def containers
      @containers ||= find_containers
    end

    def tasks
      @tasks ||= find_tasks
    end

  private

    def find_services
      filters = { label: { "com.docker.stack.namespace=#{@name}" => true } }.to_json

      Docker::Service.all(filters: filters).map do |service|
        DockerApi::Service.new(stack: self, service: service)
      end
    rescue Excon::Error => exception
      Raven.capture_exception(exception, level: :warning)

      []
    end

    def find_containers
      filters = { label: { "com.docker.stack.namespace=#{@name}" => true } }.to_json

      Docker::Container.all(filters: filters).map do |container|
        DockerApi::Container.new(stack: self, container: container)
      end
    rescue Excon::Error => exception
      Raven.capture_exception(exception, level: :warning)

      []
    end

    def find_tasks
      filters = { label: { "com.docker.stack.namespace=#{@name}" => true } }.to_json

      Docker::Task.all(filters: filters).map do |task|
        DockerApi::Task.new(stack: self, task: task)
      end
    rescue Excon::Error => exception
      Raven.capture_exception(exception, level: :warning)

      []
    end
  end
end
