# frozen_string_literal: true

module DockerApi
  class Service
    attr_reader :stack

    delegate :id, :info, to: :@service

    def initialize(stack:, service:)
      @stack = stack
      @service = service
    end

    def tasks
      @tasks ||= find_tasks
    end

    def containers
      @containers ||= find_containers
    end

    def streaming_logs(since: nil, wait: 30, &block)
      options = { stdout: true, stderr: true, follow: true, since: since, wait: wait }.compact

      @service.streaming_logs(options, &block)
    end

    def name
      info.dig("Spec", "Name")&.delete_prefix("#{stack.name}_")
    end

    def image
      info.dig("Spec", "TaskTemplate", "ContainerSpec", "Image").to_s.split("@").first.presence
    end

    def replicas
      info.dig("Spec", "Mode", "Replicated", "Replicas").to_i
    end

    def ports
      if Current.settings.traefik_version == "v2"
        traefik_services_ports
      else
        traefik_frontends_ports
      end
    end

    def container_ports
      @container_ports ||= containers.map(&:ports).flatten
    end

    def status
      healthcheck? ? status_with_healtcheck : status_without_healtcheck
    end

    def healthcheck?
      info.dig("Spec", "TaskTemplate", "ContainerSpec", "Healthcheck").present?
    end

    def created_at
      info["CreatedAt"]&.to_datetime
    end

    def updated_at
      info["UpdatedAt"]&.to_datetime
    end

  private

    def traefik_services_ports # rubocop:disable Metrics/AbcSize
      routers = info.dig("Spec", "Labels").map { |key, _| key.split(".")[3] if key.start_with?("traefik.http.routers") }.compact.uniq

      routers.map do |name|
        rule = info.dig("Spec", "Labels", "traefik.http.routers.#{name}.rule")
        service = info.dig("Spec", "Labels", "traefik.http.routers.#{name}.service")

        {
          number: info.dig("Spec", "Labels", "traefik.http.services.#{service}.loadbalancer.server.port")&.to_i,
          hosts: rule&.start_with?("Host(") ? rule.scan(/`(.+?)`/).flatten : []
        }
      end
    end

    def traefik_frontends_ports # rubocop:disable Metrics/AbcSize
      frontends = info.dig("Spec", "Labels").map { |key, _| key.split(".", 3)[1] if key.match?(/traefik\.\w+\.port/) }.compact.uniq

      frontends.map do |name|
        {
          number: info.dig("Spec", "Labels", "traefik.#{name}.port")&.to_i,
          hosts: info.dig("Spec", "Labels", "traefik.#{name}.frontend.rule").split(":", 2)[1].split(",")
        }
      end
    end

    def find_tasks
      stack.tasks.select { |task| task.service_id == id }
    end

    def find_containers
      stack.containers.select { |container| container.service_id == id }
    end

    def status_with_healtcheck
      if containers.any? && containers.all?(&:healthy?)
        :success
      elsif containers.any?(&:healthy?)
        :warning
      else
        :danger
      end
    end

    def status_without_healtcheck
      if replicas <= tasks.count(&:ok?)
        :success
      else
        :danger
      end
    end
  end
end
