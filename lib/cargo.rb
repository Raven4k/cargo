# frozen_string_literal: true

require "cargo/configuration"

module Cargo
  class << self
    def config
      @config ||= Cargo::Configuration.new
    end
  end
end
