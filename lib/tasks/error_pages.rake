# frozen_string_literal: true

namespace :error_pages do
  task generate: :environment do
    %i[not_found unprocessable_entity internal_server_error].each do |code|
      puts "Generate #{code}"
      status = Rack::Utils::SYMBOL_TO_STATUS_CODE[code]
      html = ErrorsController.render("error.html", layout: false, assigns: { code: code })

      File.open(Rails.root.join("public", "#{status}.html"), "w+") do |f|
        f.write html
      end
    end
  end
end
