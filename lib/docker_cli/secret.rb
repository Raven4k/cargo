# frozen_string_literal: true

module DockerCli
  class Secret
    class << self
      def create(name:, value:)
        stdout, stderr = Open3.capture3("docker", "secret", "create", name.downcase, "-", stdin_data: value)

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end

      def remove(name:)
        stdout, stderr = Open3.capture3("docker", "secret", "rm", name.downcase)

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end
    end
  end
end
