# frozen_string_literal: true

module DockerCli
  class Service
    class << self
      def update(name:)
        stdout, stderr = Open3.capture3("docker", "service", "update", "--force", "--detach", "--with-registry-auth", name.downcase)

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end
    end
  end
end
