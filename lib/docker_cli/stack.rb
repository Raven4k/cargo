# frozen_string_literal: true

module DockerCli
  class Stack
    class << self
      def deploy(name:, docker_compose:)
        stdout, stderr = Open3.capture3("docker", "stack", "deploy", "--prune", "--with-registry-auth", "--compose-file", "-", name.downcase, stdin_data: sanitize(docker_compose.to_yaml))

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end

      def remove(name:)
        stdout, stderr = Open3.capture3("docker", "stack", "rm", name.downcase)

        [stdout.presence, stderr.presence].compact.join("\n").chomp
      end

    private

      def sanitize(text)
        text.gsub("$", "$$")
      end
    end
  end
end
