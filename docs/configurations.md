# Configurations

## Required configuration

### Secret key base

Environment: `SECRET_KEY_BASE`

**Do not use a trivial secret for the secret_key_base, i.e. a word from a dictionary, or one which is shorter than 30 characters! Instead use `docker run --rm nuux/cargo:latest secret` to generate a secret keys!**

The secret_key_base is used as the input secret to the application's key generator, which in turn is used to create all MessageVerifiers/MessageEncryptors, including the ones that sign and encrypt cookies.

### Database url

Environment: `DATABASE_URL`

The database url to your postgres server.

### Redis url

Environment: `REDIS_URL`

The redis url to your redis server. Redis is needed for internal caching and for the web terminals.

### OpenID Connect host

Environment: `CARGO_OIDC_HOST`

The host name of the OpenID Connect instance (eg. https://gitlab.com).

### OpenID Connect identifier

Environment: `CARGO_OIDC_IDENTIFIER`  

Create a application in your OpenID Connect provider like [Gitlab](https://docs.gitlab.com/ee/integration/oauth_provider.html).  
Please select the following scopes: `openid`, `profile` and `email`  
As `Callback URL` use: https://CARGO_HOST/auth/openid_connect/callback

Use the `Application Id` for this configuration.

### OpenID Connect secret

Environment: `CARGO_OIDC_SECRET`

Use the `Secret` of the above-created OpenID Connect application for this configuration


## Optional configurations

Thes configurations are optional, if you don't provide them the default will be used.

### Sentry

Environment `SENTRY_DSN`  
Sentry will capture and send exceptions to the Sentry server whenever `SENTRY_DSN` is set.

### OpenID Connect issuer

Environment: `CARGO_OIDC_ISSUER`

The issuer name of your OpenID Connect instance (eg. https://gitlab.com).

### Stack prefix (Deprecated)

Environment: `CARGO_STACK_PREFIX`  
Default value: cargo
